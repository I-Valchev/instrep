# -*- coding: utf-8 -*-
import os
from django.conf import settings
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import authentication, permissions
from rest_framework.decorators import api_view, permission_classes
from rest_framework import viewsets, permissions

@api_view(['POST'])
@permission_classes([permissions.IsAuthenticated])
def get_file(request):
	path = request.POST.get('path') # /docs/user_uploads/...
	try:
		file_bytes = ''
		file_path = os.path.join(settings.BASE_DIR, path[1:])
		with open(file_path, 'rb') as input_file:
			data = input_file.read(1024*1024) # read in chunks of 1MB
			while data:
				file_bytes += data
				data = input_file.read(1024*1024) # returns a string
		return Response({'status': 'success', 'file_bytes': file_bytes})
	except Exception as e:
		return Response({'status': 'error', 'error': str(e)})