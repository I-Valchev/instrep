# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('complaints', '0005_fill_field_name'),
    ]

    operations = [
        migrations.AlterField(
            model_name='complaint',
            name='form',
            field=models.ForeignKey(to='forms.Field'),
        ),
    ]
