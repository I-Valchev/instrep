# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('complaints', '0026_complaint_hash_value'),
    ]

    operations = [
        migrations.AlterField(
            model_name='complaint',
            name='hash_value',
            field=models.CharField(max_length=20),
        ),
    ]
