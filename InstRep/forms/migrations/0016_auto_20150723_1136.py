# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('forms', '0015_auto_20150723_0734'),
    ]

    operations = [
        migrations.AlterField(
            model_name='field',
            name='type',
            field=models.PositiveSmallIntegerField(choices=[(1, b'text'), (2, b'text-area'), (3, b'select'), (4, b'radio'), (5, b'email'), (6, b'file'), (7, b'checkbox')]),
        ),
    ]
