
jQuery(function($) {

	//Initiat WOW JS
	new WOW().init();

    // one page navigation 
    $('.main-navigation').onePageNav({
            currentClass: 'active'
    });

    // Countdown
	$('#counter').bind('inview', function(event, visible, visiblePartX, visiblePartY) {
		if (visible) {
			$(this).find('.timer').each(function () {
				var $this = $(this);
				$({ Counter: 0 }).animate({ Counter: $this.text() }, {
					duration: 2000,
					easing: 'swing',
					step: function () {
						$this.text(Math.ceil(this.Counter));
					}
				});
			});
			$(this).unbind('inview');
		}
	});


/**
 * main.js
 * http://www.codrops.com
 *
 * Licensed under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 * 
 * Copyright 2014, Codrops
 * http://www.codrops.com
 */
(function() {

	var bodyEl = document.body,
		content = document.querySelector( '.content-wrap' ),
		openbtn = document.getElementById( 'open-button' ),
		closebtn = document.getElementById( 'close-button' ),
		outer_btn_el_firefox_fix = document.getElementById( 'glyphicon-big' );

	function init() {
		initEvents();
	}

	function initEvents() {
		if( closebtn ) {
			closebtn.addEventListener( 'click', function(ev) {
				classie.remove( bodyEl, 'show-menu' );
			} );
		}

		content.addEventListener( 'click', function(ev) {
			if((ev.target === openbtn) || (ev.target === outer_btn_el_firefox_fix)) {
				classie.add( bodyEl, 'show-menu' );
			} else {
				classie.remove( bodyEl, 'show-menu' );
			}
		} );
	}

	init();

})();

});
