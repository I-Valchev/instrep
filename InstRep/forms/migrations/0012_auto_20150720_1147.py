# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('forms', '0011_auto_20150720_1017'),
    ]

    operations = [
        migrations.AlterField(
            model_name='field',
            name='type',
            field=models.PositiveSmallIntegerField(choices=[(-1, b'undefined'), (0, b'text'), (1, b'text-area'), (2, b'select'), (3, b'radio'), (4, b'email'), (5, b'file')]),
        ),
    ]
