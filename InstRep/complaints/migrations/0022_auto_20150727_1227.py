# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('complaints', '0021_merge'),
    ]

    operations = [
        migrations.AlterField(
            model_name='fill',
            name='field_name',
            field=models.CharField(max_length=100),
        ),
    ]
