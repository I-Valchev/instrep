# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('forms', '0030_auto_20150814_1123'),
    ]

    operations = [
        migrations.AddField(
            model_name='field',
            name='html_name',
            field=models.CharField(default=-1, max_length=50, verbose_name=b'HTML name \xd0\xb0\xd1\x82\xd1\x80\xd0\xb8\xd0\xb1\xd1\x83\xd1\x82'),
            preserve_default=False,
        ),
    ]
