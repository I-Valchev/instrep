# -*- coding: utf-8 -*-
from __future__ import division
from django.db import models
from django.core.exceptions import ValidationError
from users.models import User
import datetime
from datetime import timedelta
import watson
from django.utils.version import get_complete_version

INST_STATUSES = (
    (0,'inactive'),
    (1,'pending'),
    (2,'active'),
)

def generate_filename(self, filename):
    return "%s/%s" % (self.name, filename)

class Institution(models.Model):
    name = models.CharField(max_length=200)
    user = models.ForeignKey(User)
    upload = models.FileField(upload_to=generate_filename)
    status = models.PositiveSmallIntegerField(choices=INST_STATUSES)
    notified = models.BooleanField(default=False)
    
    class Meta:
        verbose_name = u'институция'
        verbose_name_plural = u'институции'

    def get_status(self):
        return INST_STATUSES[self.status][1]

    def file_link(self):
        if self.upload:
            return "<a href='%s'>download</a>" % (self.upload.url,)
        else:
            return "No attachment"
    
    def __unicode__(self):
        return u'%s' % self.name
    
    def get_total_complaints(self):
        total_complaints = 0
        since = datetime.date.today() - timedelta(days=30)
        for form in self.form_set.all():
            total_complaints += form.complaint_set.filter(created_at__range=[since, datetime.date.today()]).count()
        return total_complaints
    
    def get_complaints_count_by_status(self, seeked_status):
        forms = self.form_set.all()
        pc_count = 0
        since = datetime.date.today() - timedelta(days=30)
        for form in forms:
            pc_count += form.complaint_set.filter(status=seeked_status, created_at__range=[since, datetime.date.today()]).count()
        return pc_count
    
    def get_pending_complaints_count(self):
        return self.get_complaints_count_by_status(0)
    
    def get_rejected_complaints_count(self):
        return self.get_complaints_count_by_status(1)
    
    def get_resolved_complaints_count(self):
        return self.get_complaints_count_by_status(2)
    
    def get_seeked_complaints_percentage(self, seeked_status):
        total_complaints = self.get_total_complaints()
        seeked_status_complaints = self.get_complaints_count_by_status(seeked_status)
        if(total_complaints):
            return ((seeked_status_complaints*100) / total_complaints)
        else:
            return 0
        
    def get_pending_complaints_percentage(self):
        return self.get_seeked_complaints_percentage(0)
    
    def get_rejected_complaints_percentage(self):
        return self.get_seeked_complaints_percentage(1)
    
    def get_resolved_complaints_percentage(self):
        return self.get_seeked_complaints_percentage(2)
    
    def save(self, **kwargs):
        self.clean()
        if self.user.permission_level == 2:
            self.user.user.is_staff = True
        return super(Institution, self).save(**kwargs)
    
    def clean(self):
        super(Institution, self).clean()
        if self.user.permission_level != 2:
            raise ValidationError('The user\'s permission level must be \'institution\'')

    file_link.allow_tags = True

# delete user associated with institution when deleting an institution - reverse cascading
from django.db.models.signals import post_delete
def delete_reverse(sender, **kwargs):
    user = kwargs['instance'].user
    if user:
        django_builtin_user = user.user
        django_builtin_user.delete()
        user.delete()
post_delete.connect(delete_reverse, sender=Institution)