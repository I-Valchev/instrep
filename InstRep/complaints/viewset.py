from rest_framework import viewsets, permissions
from .models import Complaint
from .serializer import ComplaintSerializer
from django.shortcuts import get_object_or_404
from rest_framework.decorators import list_route
from rest_framework.response import Response

from users.models import User

class ComplaintViewSet(viewsets.ModelViewSet):
    serializer_class = ComplaintSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def list(self, request):
        user = User.objects.get(user=request.user)
        queryset = Complaint.objects.all()
        if user.permission_level == 1:
            queryset = queryset.filter(user=user)
        elif user.permission_level == 2:
            queryset = queryset.filter(form__institution__user=user)
        serializer = ComplaintSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        user = User.objects.get(user=request.user)
        complaint = Complaint.objects.get(pk=pk)
        if complaint.form.institution.user == user or complaint.user == user:
            serializer = ComplaintSerializer(complaint)
            return Response(serializer.data)
        else:
            return Response({'status': 'error', 'error': 'This complaint does not belong to you.'})

    @list_route(url_path='get_by_hashcode/(?P<hashcode>[\w]+)')
    def get_by_hashcode(self, request, hashcode):
        complaint = Complaint.objects.filter(hash_value=hashcode)
        if not complaint:
            return Response({'status': 'error', 'error': 'No such complaint found.'})
        else:
            complaint = complaint[0]
            serializer = ComplaintSerializer(complaint)
            return Response(serializer.data)

    def update(self, request, *args, **kwargs):
        complaint_id = kwargs.get('pk')
        data = dict(request.data)
        user = request.user
        serializer = ComplaintSerializer()
        complaint = serializer.update(complaint_id, data, user)
        return Response(complaint)

    def get_queryset(self):
        return Complaint.objects.all()