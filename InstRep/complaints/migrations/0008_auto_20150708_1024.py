# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('complaints', '0007_auto_20150708_1020'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='complaint',
            name='form',
        ),
        migrations.DeleteModel(
            name='Fill',
        ),
        migrations.DeleteModel(
            name='Complaint',
        ),
    ]
