# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('institutions', '0003_auto_20150720_1233'),
    ]

    operations = [
        migrations.AddField(
            model_name='institution',
            name='upload',
            field=models.FileField(default=-1, upload_to=b'klj'),
            preserve_default=False,
        ),
    ]
