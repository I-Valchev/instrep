import json
from rest_framework import routers, serializers, viewsets
from .models import Form, Field, FieldOption
from institutions.models import Institution
from users.models import User

class FieldOptionSerializer(serializers.ModelSerializer):
    class Meta:
        model = FieldOption
        fields = ('option',)

    def create(self, data, field):
        data['field'] = field
        FieldOption.objects.create(**data)

class FieldSerializer(serializers.ModelSerializer):
    fieldoption_set = FieldOptionSerializer(many=True, read_only=True)
    class Meta:
        model = Field
        exclude = ('field_order', 'form', )

    def create(self, data, form):
        data['form'] = form
        field_options = data.pop('fieldoption_set', None)
        field1 = Field.objects.create(**data)
        if field_options:
            for field_option in field_options:
                FieldOptionSerializer().create(field_option, field1)

    def update(self, data):
        field_options = data.pop('fieldoption_set', None)
        if field_options:
            field = Field.objects.get(pk=data['id'])
            field.fieldoption_set.all().delete() # delete options

            for field_option in field_options: # create new ones
                FieldOptionSerializer().create(field_option, field)

        Field.objects.filter(pk=data['id']).update(**data)

class FormSerializer(serializers.ModelSerializer):
    inst_name = serializers.SerializerMethodField()
    field_set = FieldSerializer(many=True, read_only=True)

    class Meta:
        model = Form
        fields = ('id', 'name', 'inst_name', 'created_at', 'updated_at', 'main_field','field_set', )

    def get_inst_name(self, obj):
        return obj.institution.name

    def create(self, data, institution):
        try:
            data = dict(data.iterlists())
            data['institution'] = institution

            fields = data.pop('field_set', None)
            if fields:
                fields = eval(fields[0])
                form = Form.objects.create(**data)
                for field in fields:
                    FieldSerializer().create(field, form)

            return {'status': 'success'}
        except Exception as e:
            return {'status': 'error', 'error': str(e)}

    def update(self, form_id, data):
        try:
            form = Form.objects.get(pk=form_id)
            for field in form.field_set.all():
                field.delete()
            data = dict(data) # convert QuerySet to dictionary
            fields = data.pop('field_set', None) # returns a unicode string, not a list
            if fields:
                fields = eval(fields[0]) # effectively convert to list
                for field in fields:
                    FieldSerializer().create(field, form)

            # data is {'name': [u'newname']}
            data = {attr: value[0] for attr, value in data.items()} # "flatten" the data
            Form.objects.filter(pk=form_id).update(**data)
            return {'status': 'success'}
        except Exception as e:
            return {'status': 'error', 'error': str(e)}