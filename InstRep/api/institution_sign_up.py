# -*- coding: utf-8 -*-

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import authentication, permissions

from rest_framework.decorators import api_view, permission_classes
from rest_framework import viewsets, permissions

from auth.utils import user_exists, create_institution

@api_view(['POST'])
@permission_classes([permissions.IsAuthenticated])
def institution_sign_up(request):
	try:
		if user_exists(request.POST['email']):
			return Response({'status': 'error', 'error': 'Email already in use.'})
		
		create_institution(request.POST)
		return Response({'status': 'success'})
	except Exception as e:
		return Response({'status': 'error', 'error': str(e)})