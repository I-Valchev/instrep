from django import template
from users.models import User as CustomUser

register = template.Library()

@register.assignment_tag
def is_institution(user):
	if user.is_authenticated():
		if user.is_superuser:
			return False

		custom_user = CustomUser.objects.get(user=user)
		permission_level = custom_user.get_permission_level()

		if permission_level == 'regular':
			return False
		elif permission_level == 'institution':
			return True
	return False