# -*- coding: utf-8 -*-

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import authentication, permissions

from rest_framework.decorators import api_view, permission_classes
from rest_framework import viewsets, permissions

from auth.utils import create_user
from forms.models import Form
from forms.views import send_to_institution
from forms.utils import create_complaint
from users.models import User
import sys
from django.core.exceptions import ValidationError

@api_view(['POST', 'GET'])
@permission_classes([])
def form_submit(request, form_id):
    form = Form.objects.get(id=form_id)
    email = request.POST.get('email')
    if email:
        user = User.objects.get(user__email=request.POST.get('email'))
    else:
        user = None

    try:
        new_complaint = create_complaint(form, user, request.POST, request.FILES.items())
        send_to_institution(form.name, request.POST)
    except ValidationError as exception:
        return Response({'status': 'error', 'error': exception.message})

    return Response({'status': 'ok', 'complaint_id': new_complaint.id})