# -*- coding: utf-8 -*- 

from django.test import TestCase
from complaints.models import *
from django.http.response import Http404, HttpResponse
from django.test.client import RequestFactory
from complaints.models import Complaint
from forms.models import Form
from institutions.models import Institution
from django.contrib.auth.models import User as DjangoUser
from users.models import User
from forms.utils import gen_hash

class test_models(TestCase):
    
    def setUp(self):
        self.request_factory = RequestFactory()
        
        mock_django_user = DjangoUser.objects.create_user('test_user', 'test@testmail.com', 'test_password')
       	self.mock_user = User.objects.create(user=mock_django_user, permission_level=2)
        inst = Institution(user=self.mock_user, status=2, name='fake_inst_name')
        inst.save()
        test_form = Form(institution=inst)
        test_form.save()
        hash_val = gen_hash()
        self.complaint = Complaint(id=1, form = test_form, status=0, hash_value = hash_val,)
        self.complaint.save()
        self.fill = Fill(complaint=self.complaint)

    def test_complaint_str(self):
    	self.assertEqual(str(self.complaint), 'Оплакване до fake_inst_name')

    def test_fill_str(self):
    	self.assertEqual(str(self.fill), 'Поле')

    def test_generale_filename(self):
    	request = self.request_factory.get('', data={})
    	request.user = self.mock_user
    	request.user.username = 'fake_username'
    	self.assertEqual(generate_filename(request, 'fake_file'), 'files/users/fake_username/fake_file')