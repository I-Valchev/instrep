import re
from auth.utils import auto_login
from users.models import User

def change_password(request, user, new_password):
	buildin_user = user.user
	buildin_user.set_password(new_password)
	buildin_user.save()
	auto_login(request, buildin_user)

def update_profile(request, custom_user_id, request_user, data):
	user = User.objects.get(pk=custom_user_id)
	old_pass = data.get('old_pass')
	if request_user.check_password(old_pass):
		fields = ['first_name', 'last_name', 'city', 'phone', 'email', 'address', 'new_pass']
		for field in fields:
			value = data.get(field)
			if value:
				value = re.sub(r"\"", "\\\"", value) # fix character encoding problems with ""
			if value:
				if field == 'new_pass':
					change_password(request, user, value)
				else:
					exec("user.%s = \"%s\"" % (field, value))
		user.save()
		return {'status': 'success'}
	else:
		return {'status': 'error', 'error': 'Wrong password'}
