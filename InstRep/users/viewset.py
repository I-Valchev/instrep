from rest_framework import viewsets, permissions
from .models import User
from .serializer import UserSerializer
from django.shortcuts import get_object_or_404
from rest_framework.response import Response
from rest_framework.decorators import detail_route, list_route

import json

class UserViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = UserSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def list(self, request):
        return Response('Listing users not allowed. Try accessing an individual user by their id.')

    def retrieve(self, request, pk=None):
        queryset = User.objects.all()
        user = get_object_or_404(queryset, pk=pk)
        serializer = UserSerializer(user)
        return Response(serializer.data)

    @list_route(url_path='get_by_email/(?P<email>[\w.@]+)')
    def get_by_email(self, request, email):
        user = User.objects.filter(user__email=email)
        if not user:
            return Response('No such user.')
        user = user[0]
        serializer = UserSerializer(user)
        return Response(serializer.data)

    @list_route(url_path='get_by_django_user_id/(?P<django_user_id>[0-9]+)')
    def get_by_django_user_id(self, request, django_user_id):
        print(django_user_id)
        user = User.objects.filter(user__id=django_user_id)[0]
        if not user:
            return Response('No such user.')
        serializer = UserSerializer(user)
        return Response(serializer.data)
    
    def get_queryset(self):
        return User.objects.all()

    def update(self, request, *args, **kwargs):
        user_id = kwargs.get('pk')
        request_sender = User.objects.get(user=request.user)
        if request_sender.id != int(user_id):
            return Response({'status': 'error', 'error': 'You can only update your own profile!'})
        data = dict(request.data)
        serializer = UserSerializer()
        return Response(serializer.update(user_id, request.user, request, data))