# -*- coding: UTF-8 -*-

from django.test import TestCase
import unittest
from forms.views import *
from django.http.response import Http404, HttpResponse
from django.test.client import RequestFactory
import forms.models as models
from institutions.models import Institution
from forms.models import Form
from users.models import User
from django.contrib.auth.models import User as DjangoUser
from mock import patch, Mock, MagicMock
import django.core.files as djangoFile
from django.core.urlresolvers import reverse
from django.http import QueryDict

class test_views(TestCase):

    def setUp(self):
        self.request_factory = RequestFactory()

        self.mock_django_user = DjangoUser.objects.create_user('test_user', 'test@testmail.com', 'test_password')
        self.mock_user = User.objects.create(user=self.mock_django_user, permission_level=2)
        inst = Institution(user=self.mock_user, status=2, name='sth')
        inst.save()
        self.test_form = Form(institution=inst)
        self.test_form.save()
        self.field = Field(form=self.test_form, type=1, name='testname', autofill=-1)
        self.field.save()

    def test_index_fake_request(self):
        request = 'fake request'
        self.assertRaises(Http404, index, request)

    def test_index_real_request(self):
        request = self.request_factory.get('', data={})
        self.assertIsInstance(index(request), HttpResponse)

        response = self.client.get('/forms/1/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'base.html')
        self.assertTemplateUsed(response, 'forms/show.html')
        self.assertContains(response, '<input')

        request = self.request_factory.post('', data={'search': 'sth'})
        self.assertIsInstance(index(request), HttpResponse)

    def test_show_fake_data(self):
        request = 'fake request'
        self.assertRaises(Http404, show, request, 1)

        request = self.request_factory.get('', data={})
        self.assertRaises(Http404, show, request, -1)

    @patch('django.contrib.auth.models.User.is_authenticated')
    def test_show_real_data(self, mock_auth):
        mock_auth.return_value = True
        request = self.request_factory.get('/forms/1/', data={})
        request.user = self.mock_django_user
        self.assertIsInstance(show(request, 1), HttpResponse)

    @patch('django.contrib.auth.models.User.is_anonymous')
    def test_submit_fake_request(self, mock_anon):
        #wrong request
        request = 'fake request'
        self.assertRaises(Http404, submit, request, 1)

        #wrong type of request (get instead of post)
        mock_anon.return_value = True
        request = self.request_factory.get('/forms/1/', data={'failed':'request'})
        request.user = self.mock_django_user

        self.assertRaises(Http404, submit, request, 1)

        #non-existing form
        request = self.request_factory.get('/forms/15/', data={'name':'testName'})
        self.assertRaises(Http404, submit, request, 15)

    @patch('forms.views.create_complaint')
    @patch('django.contrib.auth.models.User.is_anonymous')
    def test_submit_anonymous_user(self, mock_anon, mock_create_complaint):
        request = self.request_factory.post('/forms/1/submit/', data={})
        request.user = self.mock_django_user

        mock_anon.return_value = True
        fake_complaint = MagicMock()
        fake_complaint.hash_value.return_value = 'fake_hash'
        mock_create_complaint.return_value = fake_complaint

        request.field = self.field
        self.assertIsInstance(submit(request, 1), HttpResponse)
        mock_create_complaint.assert_called_with(self.test_form, None, request.POST,
            request.FILES.items())
        mock_anon.assert_any_call()

    @patch('forms.views.create_complaint')
    @patch('django.contrib.auth.models.User.is_anonymous')
    def test_submit_regular_user(self, mock_anon, mock_create_complaint):
        mock_file = Mock(spec=djangoFile.File)
        mock_file.read.return_value = "fake file contents"
        request = self.request_factory.post('/forms/1/submit/', data={'file': mock_file})
        request.user = self.mock_django_user
        fake_complaint = MagicMock()
        fake_complaint.hash_value.return_value = 'fake_hash'
        mock_create_complaint.return_value = fake_complaint
        mock_anon.return_value = False
        request.field = self.field

        self.assertIsInstance(submit(request, 1), HttpResponse)
        mock_create_complaint.assert_called_with(self.test_form, self.mock_user,
            request.POST, request.FILES.items())
        mock_anon.assert_any_call()


    '''
    @patch('forms.views.get_postdata')
    @patch('forms.views.base64.b64encode')
    @patch('forms.views.requests.post')
    def test_send_to_institution(self, mock_post, mock_encode, mock_postdata):
        request = {'form':1, 'name':u'testName', 'csrfmiddlewaretoken':'token'}
        mock_file = MagicMock(spec=djangoFile.File)
        mock_file.name = 'docfile'
        mock_file.contents = 'wtf'
        mock_postdata.return_value = dict()
        form_name = 'Сигнал за нарушение на разпоредбите на трудовото законодателство и ЗДСл'
        query_dic = QueryDict('', mutable=True)
        query_dic.update(request)
        mock_encode.return_value = 'eyAKCiAgICAgICAgICAgICJkYXRhIjogImZha2VfZGF0YSIsIAoKICAgI'\
            'CAgICAgICAgImtleSI6ICJmYWtlX2tleSIgCgogICAgICAgIH0='

        send_to_institution(form_name, query_dic, mock_file)
        #mock_json.assert_called_with('data')
        mock_postdata.assert_called_with(query_dic, mock_file)
        mock_post.assert_called_with(
                'http://gli.gateway.bg/sitenew/newsite/irregularities/response.php',
                '{"token": "eyAKCiAgICAgICAgICAgICJkYXRhIjogImZha2VfZGF0YSIsIAoKICAgICAgICAgIC'\

                'AgImtleSI6ICJmYWtlX2tleSIgCgogICAgICAgIH0="}', auth=('user', '*****'))
    '''
