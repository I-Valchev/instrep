# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('forms', '0037_auto_20150912_1230'),
    ]

    operations = [
        migrations.AlterField(
            model_name='field',
            name='choose_one',
            field=models.PositiveSmallIntegerField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='field',
            name='description',
            field=models.TextField(default=b'', verbose_name=b'\xd0\x9e\xd0\xbf\xd0\xb8\xd1\x81\xd0\xb0\xd0\xbd\xd0\xb8\xd0\xb5 \xd0\xbd\xd0\xb0 \xd0\xbf\xd0\xbe\xd0\xbb\xd0\xb5\xd1\x82\xd0\xbe', blank=True),
        ),
        migrations.AlterField(
            model_name='field',
            name='maximum',
            field=models.IntegerField(verbose_name=b'\xd0\x9c\xd0\xb0\xd0\xba\xd1\x81\xd0\xb8\xd0\xbc\xd0\xb0\xd0\xbb\xd0\xbd\xd0\xb0 \xd0\xb3\xd0\xbe\xd0\xbb\xd0\xb5\xd0\xbc\xd0\xb8\xd0\xbd\xd0\xb0', blank=True),
        ),
        migrations.AlterField(
            model_name='field',
            name='minimum',
            field=models.IntegerField(verbose_name=b'\xd0\x9c\xd0\xb8\xd0\xbd\xd0\xb8\xd0\xbc\xd0\xb0\xd0\xbb\xd0\xbd\xd0\xb0 \xd0\xb3\xd0\xbe\xd0\xbb\xd0\xb5\xd0\xbc\xd0\xb8\xd0\xbd\xd0\xb0', blank=True),
        ),
        migrations.AlterField(
            model_name='field',
            name='validation_type',
            field=models.PositiveSmallIntegerField(default=-1, verbose_name=b'\xd0\xa2\xd0\xb8\xd0\xbf \xd0\xbd\xd0\xb0 \xd0\xb2\xd0\xb0\xd0\xbb\xd0\xb8\xd0\xb4\xd0\xb0\xd1\x86\xd0\xb8\xd1\x8f\xd1\x82\xd0\xb0', choices=[(-1, b'-'), (0, b'\xd0\xa2\xd0\xb5\xd0\xbb\xd0\xb5\xd1\x84\xd0\xbe\xd0\xbd'), (1, b'\xd0\x9f\xd0\xbe\xd1\x89\xd0\xb5\xd0\xbd\xd1\x81\xd0\xba\xd0\xb8 \xd0\xba\xd0\xbe\xd0\xb4'), (2, b'\xd0\x98\xd0\xbc\xd0\xb5'), (3, b'\xd0\xa7\xd0\xb8\xd1\x81\xd0\xbb\xd0\xbe')]),
        ),
    ]
