# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('complaints', '0031_auto_20150911_1858'),
    ]

    operations = [
        migrations.AlterField(
            model_name='complaint',
            name='form',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, verbose_name=b'\xd0\xa4\xd0\xbe\xd1\x80\xd0\xbc\xd0\xb0', to='forms.Form', null=True),
        ),
        migrations.AlterField(
            model_name='fill',
            name='field_name',
            field=models.CharField(max_length=200, verbose_name=b'\xd0\x98\xd0\xbc\xd0\xb5 \xd0\xbd\xd0\xb0 \xd0\xbf\xd0\xbe\xd0\xbb\xd0\xb5\xd1\x82\xd0\xbe'),
        ),
    ]
