# -*- coding: utf-8 -*-
from django.contrib import messages
from django.contrib.auth import authenticate
from django.contrib.auth import logout as auth_logout
from django.contrib.auth.models import User
from django.shortcuts import render
from django.shortcuts import redirect
from institutions.models import Institution
from users.models import User as CustomUser
from myapp.utils import validate_request
from .utils import auto_login
from .utils import get_username
from .utils import user_exists
from .utils import create_institution
from .utils import create_user

def is_not_logged_in(user):
	return not user.is_authenticated()

def signup(request):
	validate_request(request, 'GETPOST')
	if is_not_logged_in(request.user):
		if request.method == 'POST':
			email = username = request.POST.get('email')
			password = request.POST.get('pword')

			user = create_user(username, email, password)
			if user == 'email_taken':
				messages.error(request, 'Този имейл вече се използва!')
			else:
				messages.success(request, 'Успешно влизане в профила!')
				return auto_login(request, user)

		return render(request, 'auth/registration_form.html')
	else:
		messages.error(request, 'Вече сте в системата!')
		return redirect('/')

def login(request):
	validate_request(request, 'GETPOST')
	if is_not_logged_in(request.user):
		if request.method == 'POST':
			username = get_username(request.POST.get('email'))
			password = request.POST.get('password')
			user = authenticate(username=username, password=password)

			if user:
				if user.is_active:
					if not request.POST.get('remember_me', None):
						request.session.set_expiry(0)

					messages.success(request, 'Успешно влизане в профила!')
					return auto_login(request, user)
				else:
					messages.error(request, 'Съжаляваме! Вашият акаунт не е активен!')
			else:
				messages.error(request, 'Невалиден имейл/парола!')

		return render(request, 'auth/login.html')
	else:
		messages.error(request, 'Вече сте в системата!')

def logout(request):
	auth_logout(request)
	return redirect('/')

def institution_signup(request):
	validate_request(request, 'GETPOST')
	if is_not_logged_in(request.user):
		if request.method == 'POST':
			if user_exists(request.POST['email']):
				messages.error(request, 'Този имейл вече се използва!')
			else:
				institution = create_institution(request.POST)
				return auto_login(request, institution.user.user)

		return render(request, 'auth/institution_signup.html')
	else:
		messages.error(request, 'Вече сте в системата!')
