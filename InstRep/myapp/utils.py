from django.http.request import HttpRequest
from django.http.response import Http404

def validate_request(request, request_type):
    if not isinstance(request, HttpRequest):
        raise Http404
    elif request.method != request_type and request_type != 'GETPOST':
        raise Http404