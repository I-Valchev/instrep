# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.http import HttpResponseNotAllowed
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt

import json

from forms.models import Form, Field
from institutions.models import Institution

# Missing data in JSON:
# - name of institution - string
# - main field - string of label attribute
# - autofill (0, 'Лично име'), 
    # (1, 'Фамилия'), 
    # (2, 'Град'), 
    # (3, 'Телефон'), 
    # (4, 'Email'), 
    # (5, 'Адрес'),
# - validation type
# (-1, '-'),
#     (0, 'Телефон'), 
#     (1, 'Пощенски код'), 
#     (2, 'Име'),
#     (3, 'Число'),
# - random order

@csrf_exempt
def build_form(request):
    if request.method != 'POST':
        return HttpResponseNotAllowed('Only POST here')
    else:
        data = json.loads(request.body)

        name = data.get("title")
        forms = Form.objects.filter(name=name)
        if len(forms) > 0:
            for form in forms:
                form.delete()

        institution = Institution.objects.get(name="Инспекция по труда")
        
        form = Form(institution=institution, name=name, main_field='Описание на нарушението', approved=True)
        fields_data = data.get("properties")

        required = data.get("required")
        choose_one = data.get("choose_one")
        hidden = data.get("hidden")

        form.save()

        for field in fields_data:
            html_name = field.pop("name")
            properties = field
            field = Field(html_name=html_name)

            field.autofill = -1
            field.validation_type = -1

            if html_name != "file":
                field.name = properties.get("label")
                field.description = properties.get("description")

                field_type = properties.get("type", "string")
                if field_type == "string":
                    field.type = 1
                    field.minimum = -1
                    field.maximum = properties.get("maxLength", -1)
                elif properties["type"] == "integer":
                    field.type = 1
                    field.validation_type = 3
                    field.minimum = properties.get("minimum", -1)
                    field.maximum = properties.get("maximum", -1)

                if html_name == "fname":
                    field.autofill = 0
                    field.validation_type = 2
                elif html_name == "mname":
                    field.validation_type = 2
                elif html_name == "lname":
                    field.autofill = 1
                    field.validation_type = 2
                elif html_name == "city":
                    field.autofill = 2
                elif html_name == "phone":
                    field.autofill = 3
                    field.validation_type = 0
                elif html_name == "email":
                    field.type = 5
                    field.autofill = 4
                elif html_name == "address":
                    field.autofill == 5
                elif html_name == "postal_code":
                    field.validation_type = 1
            else:
                field_properties = properties.get("properties")[0]
                file_properties = properties.get("properties")[1]

                name = file_properties.get("name")
                field.name = field_properties.get("label")
                field.description = field_properties.get("description")
                field.type = 6 # file
                field.minimum = -1
                field.maximum = -1

            field.required = True if html_name in required else False
            field.choose_one = 1 if html_name in choose_one else -1
            if html_name in hidden:
                field.type = 8

            field.form = form
            field.save()

    return HttpResponse("Thanks!")
