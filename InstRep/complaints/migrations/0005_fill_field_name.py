# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('complaints', '0004_auto_20150708_0833'),
    ]

    operations = [
        migrations.AddField(
            model_name='fill',
            name='field_name',
            field=models.CharField(default=-1, max_length=30),
            preserve_default=False,
        ),
    ]
