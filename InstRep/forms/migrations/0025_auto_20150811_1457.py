# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('forms', '0024_auto_20150810_1359'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='field',
            options={'verbose_name': '\u041f\u043e\u043b\u0435', 'verbose_name_plural': '\u041f\u043e\u043b\u0435\u0442\u0430'},
        ),
        migrations.AlterModelOptions(
            name='form',
            options={'verbose_name': '\u0424\u043e\u0440\u043c\u0430', 'verbose_name_plural': '\u0424\u043e\u0440\u043c\u0438'},
        ),
        migrations.AlterField(
            model_name='field',
            name='autofill',
            field=models.PositiveSmallIntegerField(default=-1, help_text=b'\xd0\x90\xd0\xba\xd0\xbe \xd0\xbf\xd0\xbe\xd1\x82\xd1\x80\xd0\xb5\xd0\xb1\xd0\xb8\xd1\x82\xd0\xb5\xd0\xbb\xd1\x8f\xd1\x82 \xd0\xb5 \xd0\xbf\xd0\xbe\xd0\xbf\xd1\x8a\xd0\xbb\xd0\xbd\xd0\xb8\xd0\xbb \xd0\xbf\xd1\x80\xd0\xbe\xd1\x84\xd0\xb8\xd0\xbb\xd0\xb0 \xd1\x81\xd0\xb8 \xd1\x81 \xd0\xb2\xd1\x8a\xd0\xbf\xd1\x80\xd0\xbe\xd1\x81\xd0\xbd\xd0\xb0\xd1\x82\xd0\xb0 \xd0\xb8\xd0\xbd\xd1\x84\xd0\xbe\xd1\x80\xd0\xbc\xd0\xb0\xd1\x86\xd0\xb8\xd1\x8f, \xd0\xbf\xd0\xbe\xd0\xbb\xd0\xb5\xd1\x82\xd0\xbe \xd1\x89\xd0\xb5 \xd1\x81\xd0\xb5 \xd0\xbf\xd0\xbe\xd0\xbf\xd1\x8a\xd0\xbb\xd0\xbd\xd0\xb8 \xd0\xb0\xd0\xb2\xd1\x82\xd0\xbe\xd0\xbc\xd0\xb0\xd1\x82\xd0\xb8\xd1\x87\xd0\xbd\xd0\xbe.', verbose_name=b'\xd0\x90\xd0\xb2\xd1\x82\xd0\xbe\xd0\xbc\xd0\xb0\xd1\x82\xd0\xb8\xd1\x87\xd0\xbd\xd0\xbe \xd0\xbf\xd0\xbe\xd0\xbf\xd1\x8a\xd0\xbb\xd0\xb2\xd0\xb0\xd0\xbd\xd0\xb5', choices=[(-1, b'-'), (0, b'\xd0\x9f\xd1\x8a\xd1\x80\xd0\xb2\xd0\xbe \xd0\xb8\xd0\xbc\xd0\xb5'), (1, b'\xd0\xa4\xd0\xb0\xd0\xbc\xd0\xb8\xd0\xbb\xd0\xb8\xd1\x8f'), (2, b'\xd0\x93\xd1\x80\xd0\xb0\xd0\xb4'), (3, b'\xd0\xa2\xd0\xb5\xd0\xbb\xd0\xb5\xd1\x84\xd0\xbe\xd0\xbd'), (4, b'Email'), (5, b'\xd0\x90\xd0\xb4\xd1\x80\xd0\xb5\xd1\x81')]),
        ),
        migrations.AlterField(
            model_name='field',
            name='name',
            field=models.CharField(max_length=100, verbose_name=b'\xd0\x98\xd0\xbc\xd0\xb5 \xd0\xbd\xd0\xb0 \xd0\xbf\xd0\xbe\xd0\xbb\xd0\xb5\xd1\x82\xd0\xbe'),
        ),
        migrations.AlterField(
            model_name='field',
            name='required',
            field=models.BooleanField(default=False, verbose_name=b'\xd0\x97\xd0\xb0\xd0\xb4\xd1\x8a\xd0\xbb\xd0\xb6\xd0\xb8\xd1\x82\xd0\xb5\xd0\xbb\xd0\xbd\xd0\xbe'),
        ),
        migrations.AlterField(
            model_name='field',
            name='type',
            field=models.PositiveSmallIntegerField(verbose_name=b'\xd0\xa2\xd0\xb8\xd0\xbf \xd0\xbd\xd0\xb0 \xd0\xbf\xd0\xbe\xd0\xbb\xd0\xb5\xd1\x82\xd0\xbe', choices=[(1, b'\xd0\xa2\xd0\xb5\xd0\xba\xd1\x81\xd1\x82\xd0\xbe\xd0\xb2\xd0\xbe \xd0\xbf\xd0\xbe\xd0\xbb\xd0\xb5'), (2, b'\xd0\xa2\xd0\xb5\xd0\xba\xd1\x81\xd1\x82'), (3, b'\xd0\x9f\xd0\xb0\xd0\xb4\xd0\xb0\xd1\x89\xd0\xbe \xd0\xbf\xd0\xbe\xd0\xbb\xd0\xb5'), (4, b'\xd0\x91\xd1\x83\xd1\x82\xd0\xbe\xd0\xbd\xd0\xb8 \xd0\xb7\xd0\xb0 \xd0\xb5\xd0\xb4\xd0\xb8\xd0\xbd\xd0\xb8\xd1\x87\xd0\xb5\xd0\xbd \xd0\xb8\xd0\xb7\xd0\xb1\xd0\xbe\xd1\x80'), (5, b'Email'), (6, b'\xd0\xa4\xd0\xb0\xd0\xb9\xd0\xbb'), (7, b'\xd0\x9a\xd1\x83\xd1\x82\xd0\xb8\xd0\xb9\xd0\xba\xd0\xb0 \xd0\xb7\xd0\xb0 \xd0\xb8\xd0\xb7\xd0\xb1\xd0\xbe\xd1\x80')]),
        ),
        migrations.AlterField(
            model_name='form',
            name='approved',
            field=models.BooleanField(default=False, verbose_name=b'\xd0\x9e\xd0\xb4\xd0\xbe\xd0\xb1\xd1\x80\xd0\xb5\xd0\xbd\xd0\xb0'),
        ),
        migrations.AlterField(
            model_name='form',
            name='name',
            field=models.CharField(max_length=50, verbose_name=b'\xd0\x98\xd0\xbc\xd0\xb5 \xd0\xbd\xd0\xb0 \xd1\x84\xd0\xbe\xd1\x80\xd0\xbc\xd0\xb0\xd1\x82\xd0\xb0'),
        ),
    ]
