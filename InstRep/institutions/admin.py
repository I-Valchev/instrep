# -*- coding: utf-8 -*-
from django.contrib import admin
from django.core.mail import send_mail
from institutions.models import Institution
from .forms import InstitutionForm
from myapp.utils import validate_request


class InstitutionAdmin(admin.ModelAdmin):
    list_filter = ('status',)
    list_display = ('name', 'status',)
    actions = ['activate_inst']
    form = InstitutionForm

    def activate_inst(self, request, queryset):
        validate_request(request, 'POST')
        for i in queryset:
            i.status = 2
            i.save()
    activate_inst.short_description = u'Активирай институция'

    def get_actions(self, request):
        validate_request(request, 'GETPOST')
        actions = super(InstitutionAdmin, self).get_actions(request)
        if not request.user.is_superuser:
            del actions['activate_inst']
        return actions

    def save_model(self, request, obj, institution, change):
        validate_request(request, 'GETPOST')
        if obj.status == 2:
            subject = 'Активация на акаунт в ПодайСигнал'
            body = 'Вашият акаунт като институция беше потвърден! Вече можете да създавате форми и да отговаряте на оплаквания.'
            sender = 'stamaniorec@gmail.com'
            receiver = [obj.user.user.email]
            send_mail(subject, body, sender, receiver)
        obj.save()

admin.site.register(Institution, InstitutionAdmin)
