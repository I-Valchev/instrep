from django.conf.urls import url, patterns

urlpatterns = patterns( 'complaints',
    url(r'^(?P<complaint_id>[0-9]+)/$', 'views.show', name='show'),
    url(r'^(?P<hash_value>\w+)/$', 'views.hash_show', name='hash_show'),
)