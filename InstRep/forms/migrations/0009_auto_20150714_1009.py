# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('institutions', '0001_initial'),
        ('forms', '0008_auto_20150713_1112'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='form',
            name='inst_name',
        ),
        migrations.AddField(
            model_name='form',
            name='institution',
            field=models.ForeignKey(default=-1, to='institutions.Institution'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='form',
            name='name',
            field=models.CharField(default=-1, max_length=50),
            preserve_default=False,
        ),
    ]
