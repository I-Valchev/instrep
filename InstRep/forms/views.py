# -*- coding: utf-8 -*-
import os
import watson
from complaints.models import Complaint
from django.http.response import Http404
from django.shortcuts import render, redirect
from institutions.models import Institution
from myapp.utils import validate_request
from users.models import User
from .models import Form, Field
from .utils import build_form_query_result, gen_hash, create_complaint, get_ip

def index(request):
    validate_request(request, 'GETPOST')

    forms_set = Form.objects.all()
    if request.method == 'POST':
        query = request.POST.get('search')
        if query:
            forms_set = build_form_query_result(query)

    return render(request, 'forms/index.html', {'forms': forms_set})

def show(request, form_id):
    validate_request(request, 'GET')
    try:
        form = Form.objects.get(id=form_id)
    except:
        raise Http404
    context =  {'form': form, 'fields': form.field_set.all(), 'ip': get_ip(request)}
    if request.user.is_authenticated():
        context['custom_user'] = User.objects.get(user=request.user)
    return render(request, 'forms/show.html', context)

def submit(request, form_id):
    validate_request(request, 'POST')

    form = Form.objects.get(id=form_id)
    if request.user.is_anonymous():
        user = None
    else:
        user = User.objects.get(user__email=request.user.email)

    new_complaint = create_complaint(form, user, request.POST, request.FILES.items())
    send_to_institution(form.name, request.POST, request.FILES)

    return redirect("/complaints/%s" % new_complaint.hash_value)

import base64
from datetime import datetime
import json
import jwt
import requests
import uuid
from .utils import files_to_dicts

def send_to_institution(form_name, request_data, files):
    if form_name == u"Сигнал за нарушение на разпоредбите на трудовото законодателство и ЗДСл":
        del request_data["csrfmiddlewaretoken"]
        post_data = dict(request_data.iterlists()) # request.POST is a QueryDict, I want a normal python dict
        for file_dict in files_to_dicts(form_name, files):
            post_data.update(file_dict)
        json_string = json.dumps(post_data)

        # print(json_string)

        token = {
            "iss": "http://podaisignal.herokuapp.com",
            "aud": "http://gli.government.bg",
            "iat": int(datetime.utcnow().strftime("%s")),
            "nbf": int(datetime.utcnow().strftime("%s")),
            "exp": int(datetime.utcnow().strftime("%s")),
            "msg": base64.b64encode(json_string)
        }

        key =  str(uuid.uuid4().hex.upper()[0:6]) # random string
        data = jwt.encode(token, key, algorithm='HS256')

        send_data = """{
            "data": "%s",
            "key": "%s"
        }""" % (data, key)

        encoded_data = base64.b64encode(send_data)
        response = requests.post('http://gli.gateway.bg/sitenew/newsite/irregularities/response.php', data = {"token": encoded_data})
        # print(response.text) # => "SAVE OK"
        # print(response.status_code) # => 200