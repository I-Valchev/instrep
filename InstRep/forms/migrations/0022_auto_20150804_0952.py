# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('forms', '0021_field_autofill'),
    ]

    operations = [
        migrations.AlterField(
            model_name='field',
            name='autofill',
            field=models.PositiveSmallIntegerField(default=-1, choices=[(-1, b'none'), (0, b'first_name'), (1, b'last_name'), (2, b'city'), (3, b'phone'), (4, b'email'), (5, b'address')]),
        ),
    ]
