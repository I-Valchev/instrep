# -*- coding: UTF-8 -*-

from django.test import TestCase
from users.models import *
from users import models
import unittest
from django.contrib.auth.models import User as DjangoUser
from mock import patch
from django.core.exceptions import ValidationError

class test_models(TestCase):
    def setUp(self):
        self.mock_django_user = DjangoUser.objects.create_user('test_user', 'test@testmail.com', 'test_password')
        self.mock_user = User.objects.create(user=self.mock_django_user, permission_level=1)

    def test_get_permission_level(self):
        self.mock_user.permission_level = 1
        self.assertEqual(self.mock_user.get_permission_level(), 'regular')

        self.mock_user.permission_level = 2
        self.assertEqual(self.mock_user.get_permission_level(), 'institution')

        self.mock_user.permission_level = 3
        self.assertEqual(self.mock_user.get_permission_level(), 'superadmin')

    @patch('users.models.User.clean')
    def test_clean(self, mock_save):
        self.mock_user.save()
        mock_save.assert_any_call()

    @patch('django.db.models.Model.clean')
    def test_clean(self, mock_django_clean):
        self.mock_user.clean()
        mock_django_clean.assert_any_call()

    def test_to_str(self):
        self.assertEqual(str(self.mock_user), 'test@testmail.com')
