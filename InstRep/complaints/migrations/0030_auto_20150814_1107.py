# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('complaints', '0029_auto_20150811_1457'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='comment',
            options={'verbose_name': '\u043a\u043e\u043c\u0435\u043d\u0442\u0430\u0440', 'verbose_name_plural': '\u043a\u043e\u043c\u0435\u043d\u0442\u0430\u0440\u0438'},
        ),
        migrations.AlterModelOptions(
            name='complaint',
            options={'verbose_name': '\u043e\u043f\u043b\u0430\u043a\u0432\u0430\u043d\u0435', 'verbose_name_plural': '\u043e\u043f\u043b\u0430\u043a\u0432\u0430\u043d\u0438\u044f'},
        ),
        migrations.AlterModelOptions(
            name='fill',
            options={'verbose_name': '\u043f\u043e\u043b\u0435', 'verbose_name_plural': '\u043f\u043e\u043b\u0435\u0442\u0430'},
        ),
        migrations.AlterField(
            model_name='complaint',
            name='form',
            field=models.ForeignKey(verbose_name=b'\xd0\xa4\xd0\xbe\xd1\x80\xd0\xbc\xd0\xb0', to='forms.Form'),
        ),
        migrations.AlterField(
            model_name='fill',
            name='val',
            field=models.CharField(max_length=200, null=True, verbose_name=b'\xd0\xa1\xd1\x82\xd0\xbe\xd0\xb9\xd0\xbd\xd0\xbe\xd1\x81\xd1\x82 \xd0\xbd\xd0\xb0 \xd0\xbf\xd0\xbe\xd0\xbb\xd0\xb5\xd1\x82\xd0\xbe', blank=True),
        ),
    ]
