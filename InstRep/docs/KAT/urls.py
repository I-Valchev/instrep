from django.conf.urls import url, patterns

urlpatterns = patterns( 'forms',
    url(r'^$', 'views.index', name='index'),
    url(r'^(?P<form_id>[0-9]+)/$', 'views.show', name='show'),
    url(r'^(?P<form_id>[0-9]+)/submit/$', 'views.submit', name='submit'),
)