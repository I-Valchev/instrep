from rest_framework import viewsets, permissions
from rest_framework.decorators import detail_route
from .models import Form
from .serializer import FormSerializer

from complaints.serializer import ComplaintSerializer
from complaints.models import Complaint
from forms.models import Form
from institutions.models import Institution
from users.models import User

from rest_framework.response import Response

from .serializer import FormSerializer

class FormViewSet(viewsets.ModelViewSet):
	serializer_class = FormSerializer
	permission_classes = (permissions.IsAuthenticated,)

	def get_queryset(self):
		queryset = Form.objects.filter(approved=True)
		user = User.objects.get(user=self.request.user)
		if user and user.permission_level == 2:
			queryset = queryset.filter(institution__user=user)
		return queryset

	def create(self, request, *args, **kwargs):
		user = User.objects.get(user=request.user)
		institution = Institution.objects.get(user=user)

		serializer = FormSerializer()
		form = serializer.create(self.request.POST, institution)

		return Response(form)

	def update(self, request, *args, **kwargs):
		user = User.objects.get(user=request.user)
		form_id = kwargs.get('pk')
		form = Form.objects.get(pk=form_id)
		if form.institution.user != user:
			return Response({'status': 'error', 'error': 'This form does not belong to you.'})

		serializer = FormSerializer()
		form = serializer.update(form_id, dict(request.data))
		return Response(form)

	@detail_route(url_path='complaints')
	def get_complaints(self, request, pk=None):
		user = User.objects.get(user=request.user)
		queryset = Complaint.objects.filter(user=user)
		serializer = ComplaintSerializer(queryset, many=True)
		return Response(serializer.data)