# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import complaints.models


class Migration(migrations.Migration):

    dependencies = [
        ('complaints', '0030_auto_20150814_1107'),
    ]

    operations = [
        migrations.AlterField(
            model_name='file',
            name='val',
            field=models.FileField(null=True, upload_to=complaints.models.generate_filename, blank=True),
        ),
    ]
