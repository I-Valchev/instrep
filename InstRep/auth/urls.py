from django.conf.urls import include, url

urlpatterns = [
    url(r'^signup', 'auth.views.signup', name='signup'),
    url(r'^login', 'auth.views.login', name='login'),
    url(r'^logout', 'auth.views.logout', name='logout'),
    url(r'^inst_signup', 'auth.views.institution_signup', name='inst_signup'),
    url(r'', include('allauth.urls')),
]
