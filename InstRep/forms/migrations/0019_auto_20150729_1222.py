# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('forms', '0018_form_main_field'),
    ]

    operations = [
        migrations.AddField(
            model_name='form',
            name='approved',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='form',
            name='main_field',
            field=models.PositiveSmallIntegerField(max_length=200, choices=[(1, b'text'), (2, b'text-area'), (3, b'select'), (4, b'radio'), (5, b'email'), (6, b'file'), (7, b'checkbox')]),
        ),
    ]
