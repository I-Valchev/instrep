# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('complaints', '0025_file_val'),
    ]

    operations = [
        migrations.AddField(
            model_name='complaint',
            name='hash_value',
            field=models.CharField(default=-1, max_length=10),
            preserve_default=False,
        ),
    ]
