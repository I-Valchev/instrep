from django.db import models
from django.contrib.auth.models import User as DjangoUser
from django.core.exceptions import ValidationError
from django.db.models.fields import CharField

USER_LEVELS = (
    (1, 'regular'),
    (2, 'institution'),
    (3, 'superadmin'),
)

class User(models.Model):
    user = models.OneToOneField(DjangoUser)
    permission_level = models.PositiveSmallIntegerField(choices = USER_LEVELS)
    
    def get_permission_level(self):
        return USER_LEVELS[self.permission_level-1][1]

    def save(self, **kwargs):
        self.clean()
        return super(User, self).save(**kwargs)
    
    def clean(self):
        super(User, self).clean()
        if USER_LEVELS[self.permission_level-1][1] != 'regular' and USER_LEVELS[self.permission_level-1][1] != 'institution' and USER_LEVELS[self.permission_level-1][1] != 'superadmin':
            raise ValidationError('%s is not a valid permission level.' % self.permission_level)

    def __unicode__(self):
        return self.user.email
    
    first_name = models.CharField(max_length=30, blank=True, null=True)
    last_name = models.CharField(max_length=30, blank=True, null=True)
    city = models.CharField(max_length=30, blank=True, null=True)
    phone = models.CharField(max_length=12, blank=True, null=True)
    email = models.CharField(max_length=30, blank=True, null=True)
    address = models.CharField(max_length=80, blank=True, null=True)

from django.dispatch import receiver
from allauth.account.signals import user_signed_up
@receiver(user_signed_up)
def user_signed_up_(request, user, **kwargs):
    User.objects.create(user=user, permission_level=1)
