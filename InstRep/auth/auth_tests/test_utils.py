# -*- coding: UTF-8 -*-

from django.test import TestCase
from auth.utils import *
from mock import patch, Mock
from django.test.client import RequestFactory

class test_views(TestCase):

	def setUp(self):
		self.request_factory = RequestFactory()

	@patch('auth.utils.User.objects.filter')
	def test_user_exists(self, mock_filter):
		mock_users = Mock()
		mock_users.count.return_value = 1
		mock_filter.return_value = mock_users

		self.assertTrue(user_exists('fake_mail'))
		mock_filter.assert_called_with(email='fake_mail')

	@patch('auth.utils.User.objects.filter')
	def test_user_exists_false(self, mock_filter):
		mock_users = Mock()
		mock_users.count.return_value = 0
		mock_filter.return_value = mock_users

		self.assertFalse(user_exists('fake_mail'))
		mock_filter.assert_called_with(email='fake_mail')

	@patch('auth.utils.User.objects.get')
	@patch('auth.utils.user_exists')
	def test_get_username_existing_user(self, mock_user_exists, mock_get):
		mock_user_exists.return_value = True
		mock_user = Mock(username='test_username')
		mock_get.return_value = mock_user

		self.assertEqual(get_username('test_email'), 'test_username')
		mock_get.assert_called_with(email='test_email')
		mock_user_exists.assert_called_with('test_email')

	@patch('auth.utils.redirect_accordingly')
	@patch('auth.utils.login')
	def test_auto_login(self, mock_django_login, mock_redirect):
		request = self.request_factory.post('', data={})
		mock_user = Mock()

		auto_login(request, mock_user)

		mock_django_login.assert_called_with(request, mock_user)
		mock_redirect.assert_called_with(mock_user)

	@patch('auth.utils.redirect')
	def test_redirect_accordingly_unauthenticated(self, mock_redirect):
		mock_user = Mock()
		mock_user.is_authenticated.return_value = False
		redirect_accordingly(mock_user)

		mock_redirect.assert_called_with('/')

	@patch('auth.utils.redirect')
	def test_redirect_accordingly_authenticated_admin(self, mock_redirect):
		mock_user = Mock()
		mock_user.is_authenticated.return_value = True
		mock_user.is_superuser = True

		redirect_accordingly(mock_user)

		mock_redirect.assert_called_with('/admin/')

	@patch('auth.utils.CustomUser.objects.get')
	@patch('auth.utils.redirect')
	def test_redirect_accordingly_authenticated_user(self, mock_redirect, mock_get_custom_user):
		mock_user = Mock()
		mock_user.is_authenticated.return_value = True
		mock_user.is_superuser = False
		mock_custom_user = Mock()
		mock_custom_user.get_permission_level.return_value = 'regular'
		mock_get_custom_user.return_value = mock_custom_user

		redirect_accordingly(mock_user)

		mock_custom_user.get_permission_level.assert_any_call()
		mock_redirect.assert_called_with('dashboard')

	@patch('auth.utils.CustomUser.objects.get')
	@patch('auth.utils.redirect_institution_user')
	def test_redirect_accordingly_authenticated_inst(self, mock_redirect_inst, mock_get_custom_user):
		mock_user = Mock()
		mock_user.is_authenticated.return_value = True
		mock_user.is_superuser = False
		mock_custom_user = Mock()
		mock_custom_user.get_permission_level.return_value = 'institution'
		mock_get_custom_user.return_value = mock_custom_user

		redirect_accordingly(mock_user)

		mock_custom_user.get_permission_level.assert_any_call()
		mock_redirect_inst.assert_called_with(mock_custom_user)

	@patch('auth.utils.Institution.objects.get')
	@patch('auth.utils.redirect')
	def test_redirect_institution_user_unsubmitted_file(self, mock_redirect, mock_get_inst):
		mock_inst = Mock()
		mock_inst.status = 0
		mock_inst.id = 1
		mock_get_inst.return_value = mock_inst

		redirect_institution_user(mock_inst)

		mock_redirect.assert_called_with('/upload/1')

	@patch('auth.utils.Institution.objects.get')
	@patch('auth.utils.redirect')
	def test_redirect_institution_user_submitted_file(self, mock_redirect, mock_get_inst):
		mock_inst = Mock()
		mock_inst.status = 1
		mock_get_inst.return_value = mock_inst

		redirect_institution_user(mock_inst)

		mock_redirect.assert_called_with('/thanks_for_submission')

	@patch('auth.utils.Institution.objects.get')
	@patch('auth.utils.redirect')
	def test_redirect_institution_user_submitted_file(self, mock_redirect, mock_get_inst):
		mock_inst = Mock()
		mock_inst.status = 2
		mock_get_inst.return_value = mock_inst

		redirect_institution_user(mock_inst)

		mock_redirect.assert_called_with('/admin/')

	@patch('auth.utils.user_exists')
	def test_create_user_existing(self, mock_user_exists):
		mock_user_exists.return_value = True

		self.assertEqual(create_user('fake_username', 'fake@gmail.com', 'fake_pass'), 'email_taken')

	@patch('auth.utils.CustomUser.objects.create')
	@patch('auth.utils.User.objects.create_user')
	@patch('auth.utils.user_exists')
	def test_create_user_not_existing(self, mock_user_exists, mock_create_user, mock_create_custom_user):
		mock_user_exists.return_value = False
		mock_user = Mock()
		mock_create_user.return_value = mock_user

		create_user('username', 'email@gmail.com', 'password')

		mock_user_exists.assert_called_with('email@gmail.com')
		mock_create_user.assert_called_with('username', 'email@gmail.com', 'password')
		mock_create_custom_user.assert_called_with(user=mock_user, permission_level=1)

	@patch('auth.utils.Institution.objects.create')
	@patch('auth.utils.Group.objects.get')
	@patch('auth.utils.CustomUser.objects.create')
	@patch('auth.utils.User.objects.create_user')
	def test_create_institution(self, mock_create_user, mock_create_custom_user,
	 		mock_group_get, mock_create_inst):
		mock_user = Mock()
		mock_create_user.return_value = mock_user
		mock_custom_user = Mock()
		mock_create_custom_user.return_value = mock_custom_user
		mock_group = Mock()
		mock_group_get.return_value = mock_group
		post_data = {'email':'inst@gmail.com', 'passwd':'complete_secret',
			'phone':'0123', 'address':'pozitano','name':'inst_name'}

		create_institution(post_data)


		mock_create_user.assert_called_with('inst@gmail.com', 'inst@gmail.com',
			'complete_secret')
		mock_create_custom_user.assert_called_with(user=mock_user, phone='0123',
		 	address='pozitano', permission_level=2)
		mock_group_get.assert_called_with(name='staff')
		mock_group.user_set.add.assert_called_with(mock_user)
		mock_create_inst.assert_called_with(user=mock_custom_user, name='inst_name', status=0)
		self.assertEqual(mock_user.is_staff, True)
		mock_user.save.assert_any_call()
