# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('forms', '0011_auto_20150722_0629'),
        ('forms', '0013_auto_20150720_1308'),
        ('forms', '0011_auto_20150722_0915'),
    ]

    operations = [
    ]
