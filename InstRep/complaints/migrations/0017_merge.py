# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('complaints', '0016_auto_20150722_0629'),
        ('complaints', '0016_auto_20150720_1308'),
        ('complaints', '0016_auto_20150722_0915'),
    ]

    operations = [
    ]
