# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('forms', '0004_auto_20150707_1052'),
        ('complaints', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='complaints',
            name='form_id',
            field=models.ForeignKey(default=-1, to='forms.Form'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='fills',
            name='val',
            field=models.CharField(default=-1, max_length=200),
            preserve_default=False,
        ),
    ]
