from django.test import TestCase
from complaints.views import *
from django.http.response import Http404, HttpResponse
from django.test.client import RequestFactory
from complaints.models import Complaint
from forms.models import Form
from institutions.models import Institution
from django.contrib.auth.models import User as DjangoUser
from users.models import User
from forms.utils import gen_hash

class test_views(TestCase):

    def setUp(self):
        self.request_factory = RequestFactory()

        mock_django_user = DjangoUser.objects.create_user('test_user', 'test@testmail.com', 'test_password')
        mock_user = User.objects.create(user=mock_django_user, permission_level=2)
        inst = Institution(user=mock_user, status=2)
        inst.save()
        test_form = Form(institution=inst)
        test_form.save()
        hash_val = gen_hash()
        self.complaint = Complaint(id=1, form = test_form, status=0, hash_value = hash_val)
        self.complaint.save()

    def test_show_wrong_data(self):
        request = 123
        self.assertRaises(Http404, show, request, complaint_id=1)

    def test_show_get_request(self):
        response = self.client.get('/complaints/' + str(self.complaint.hash_value) + '/')
        self.assertTemplateUsed(response, 'complaints/show.html')
        self.assertContains(response, '<button')

    def test_show_post_request(self):
        request = self.request_factory.post('', data={'comment_text':'fake_comment'})
        result = show(request, 1)
        self.assertContains(result, 'fake_comment')

    def test_hash_show_real_data(self):
        request = self.request_factory.post('', data={'comment_text':'Comment sample'})
        self.assertIsInstance(hash_show(request, self.complaint.hash_value), HttpResponse)
        response = self.client.get('/complaints/' + str(self.complaint.hash_value) +'/')
        self.assertContains(response, 'Comment sample')
