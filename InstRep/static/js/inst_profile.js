$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});

$("#content").find(".navbar").hide();

// black magic to detect changes in form with confirmations and things
// DO NOT TOUCH
dataChanged = 0;
function bindForChange(){
     $('input,checkbox,textarea,radio,select').bind('change',function(event) { dataChanged = 1})
     $(':reset,:submit').bind('click',function(event) { dataChanged = 0 })
}
function askConfirm(){
    if (dataChanged){
        return "You have some unsaved changes.  Press OK to continue without saving."
    }
}
window.onbeforeunload = askConfirm;
window.onload = bindForChange;
