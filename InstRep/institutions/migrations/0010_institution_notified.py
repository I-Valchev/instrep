# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('institutions', '0009_auto_20150814_1107'),
    ]

    operations = [
        migrations.AddField(
            model_name='institution',
            name='notified',
            field=models.BooleanField(default=False),
        ),
    ]
