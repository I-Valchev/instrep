# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('forms', '0038_auto_20150912_1320'),
    ]

    operations = [
        migrations.AlterField(
            model_name='field',
            name='choose_one',
            field=models.PositiveSmallIntegerField(default=-1, blank=True),
        ),
        migrations.AlterField(
            model_name='field',
            name='maximum',
            field=models.IntegerField(default=-1, verbose_name=b'\xd0\x9c\xd0\xb0\xd0\xba\xd1\x81\xd0\xb8\xd0\xbc\xd0\xb0\xd0\xbb\xd0\xbd\xd0\xb0 \xd0\xb3\xd0\xbe\xd0\xbb\xd0\xb5\xd0\xbc\xd0\xb8\xd0\xbd\xd0\xb0', blank=True),
        ),
        migrations.AlterField(
            model_name='field',
            name='minimum',
            field=models.IntegerField(default=-1, verbose_name=b'\xd0\x9c\xd0\xb8\xd0\xbd\xd0\xb8\xd0\xbc\xd0\xb0\xd0\xbb\xd0\xbd\xd0\xb0 \xd0\xb3\xd0\xbe\xd0\xbb\xd0\xb5\xd0\xbc\xd0\xb8\xd0\xbd\xd0\xb0', blank=True),
        ),
    ]
