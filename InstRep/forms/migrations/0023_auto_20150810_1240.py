# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('forms', '0022_auto_20150804_0952'),
    ]

    operations = [
        migrations.RenameField(
            model_name='field',
            old_name='type',
            new_name='field_type',
        ),
    ]
