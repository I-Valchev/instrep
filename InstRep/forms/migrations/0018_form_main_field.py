# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('forms', '0017_merge'),
    ]

    operations = [
        migrations.AddField(
            model_name='form',
            name='main_field',
            field=models.CharField(default=-1, max_length=200),
            preserve_default=False,
        ),
    ]
