# -*- coding: UTF-8 -*-

from django.test import TestCase
from institutions.models import *
from users.models import User
from django.contrib.auth.models import User as DjangoUser
from django.core.files.uploadedfile import SimpleUploadedFile
from forms.utils import gen_hash
from complaints.models import Complaint
from forms.models import Form
from mock import patch
import datetime

class test_models(TestCase):
    def setUp(self):
        mock_django_user = DjangoUser.objects.create_user('test_user', 'test@testmail.com', 'test_password')
        mock_user = User.objects.create(user=mock_django_user, permission_level=2)
        self.inst = Institution(name='TestInst', user=mock_user, status=2)
        self.inst.save()

    def test_statuses(self):
    	self.inst.status = 2
    	self.assertEqual(self.inst.get_status(), 'active')

    	self.inst.status = 1
    	self.assertEqual(self.inst.get_status(), 'pending')

    	self.inst.status = 0
    	self.assertEqual(self.inst.get_status(), 'inactive')

    def test_file_link_no_upload(self):
    	self.inst.upload = False
    	self.assertEqual(self.inst.file_link(), 'No attachment')

    def test_file_link_with_upload(self):
    	self.inst.upload = SimpleUploadedFile('mock_filename', 'mock_content')
    	self.assertEqual(self.inst.file_link(), "<a href='/docs/mock_filename'>download</a>")

    def test_generate_filename(self):
    	self.assertEqual(generate_filename(self.inst, 'fake_filename'), 'TestInst/fake_filename')

    @patch('institutions.models.Institution.get_total_complaints')
    @patch('institutions.models.Institution.get_complaints_count_by_status')
    def test_get_seeked_complaints_percentage_not_zero(self, mock_get_complaints, mock_total_complaints):
    	mock_total_complaints.return_value = 4
    	mock_get_complaints.return_value = 2
    	self.assertEqual(self.inst.get_seeked_complaints_percentage(0), 50)

    def test_clean_wrong_permission_level(self):
    	mock_django_user = DjangoUser.objects.create_user('another_test', 'another@testmail.com', 'notpass')
        mock_user = User.objects.create(user=mock_django_user, permission_level=1)
        another_inst = Institution(name='AnotherInst', user=mock_user)
        self.assertRaises(ValidationError, another_inst.clean)

    @patch('institutions.models.User.delete')
    @patch('django.contrib.auth.models.User.delete')
    def test_delete_reverse(self, mock_builtin_delete, mock_delete):
        delete_reverse(self.inst, instance=self.inst)
        mock_builtin_delete.assert_any_call()
        mock_delete.assert_any_call()
