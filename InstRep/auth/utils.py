from django.contrib.auth import login
from django.contrib.auth.models import User, Group
from django.shortcuts import redirect
from institutions.models import Institution
from users.models import User as CustomUser

def user_exists(email):
	return User.objects.filter(email=email).count() > 0

def get_username(email):
	return User.objects.get(email=email).username if user_exists(email) else None

def auto_login(request, user):
	user.backend = "django.contrib.auth.backends.ModelBackend" # needed on signup for login
	login(request, user)
	return redirect_accordingly(user)

def redirect_accordingly(user):
	if not user.is_authenticated():
		return redirect('/')

	if user.is_superuser:
		return redirect('/admin/')

	custom_user = CustomUser.objects.get(user=user)
	permission_level = custom_user.get_permission_level()

	if permission_level == 'regular':
		return redirect('dashboard')
	elif permission_level == 'institution':
		return redirect_institution_user(custom_user)

def redirect_institution_user(user):
	institution = Institution.objects.get(user=user)
	if institution.status == 0:
		return redirect('/upload/%d' % institution.id)
	elif institution.status == 1:
		return redirect('/thanks_for_submission')
	else:
		return redirect('/admin/')

def create_user(username, email, password):
	if user_exists(email):
		return 'email_taken'
	user = User.objects.create_user(username, email, password)
	CustomUser.objects.create(user=user, permission_level=1)
	return user

def create_institution(post_data):
	user = User.objects.create_user(
		post_data['email'], # username
		post_data['email'],
		post_data['passwd']
	)
	user.is_staff = True
	user.save()

	custom_user = CustomUser.objects.create(
		user=user,
		phone=post_data.get('phone'),
		address=post_data.get('address'),
		permission_level=2
	)

	inst_group = Group.objects.get(name='staff')
	inst_group.user_set.add(user)

	return Institution.objects.create(
		user=custom_user,
		name=post_data['name'],
		status=0
	)
