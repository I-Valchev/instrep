from django.contrib import admin
from django.contrib.auth.models import User as DjangoUser
from django.contrib.auth.admin import UserAdmin as DjangoAdmin
from .models import User

class UserInline(admin.StackedInline):
    model = User
    can_delete = False

class DjangoUserAdmin(DjangoAdmin):
    inlines = (UserInline,)

admin.site.unregister(DjangoUser)
admin.site.register(DjangoUser, DjangoUserAdmin)
