# -*- coding: utf-8 -*-
from django.contrib import messages
from django.http import HttpResponseRedirect
from complaints.models import Complaint
from users.models import User

import re

class RestrictComplaintMiddleware(object):
    def process_request(self, request):
        trying_to_access_complaint = re.match(r'^/complaints/\d+/$', request.path)
        if trying_to_access_complaint:
            if not self.belongs_to_me(request.user, request.path.split('/')[-2]):
                messages.error(request, 'Съжаляваме! Няма такъв сигнал!')
                return HttpResponseRedirect('/')

    def belongs_to_me(self, user, complaint_id):
        if user.is_authenticated():
            if user.is_superuser:
                return True

            owner = User.objects.get(user=user)
            try:
                complaint = Complaint.objects.get(id=complaint_id)
                if complaint.user == owner:
                    return True
            except:
                pass
        return False

class EnsureComplaintByHashMiddleware(object):
    def process_request(self, request):
        trying_to_access_complaint = re.compile('^/complaints/(\w{20})/$').search(request.path)
        if trying_to_access_complaint:
            print("in here!")
            hash_value = trying_to_access_complaint.groups()[0]
            try:
                Complaint.objects.get(hash_value=hash_value)
            except:
                messages.error(request, 'Съжаляваме! Не открихме сигнал с дадения хеш код!')
                return HttpResponseRedirect('/')