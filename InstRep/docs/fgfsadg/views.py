from django.shortcuts import render
from .models import Form
from complaints.models import Complaint
#from django.core.context_processors import request

def index(request):
    return render(request, 'forms/index.html', {'forms': Form.objects.all()})

def show(request, form_id):
    form = Form.objects.get(id=form_id)
    return render(request, 'forms/show.html', {'form': form, 'fields': form.field_set.all()})

def submit(request, form_id):
    form = Form.objects.get(id=form_id)
    new_complaint = Complaint(form = form)
    new_complaint.save()
    for key, value in request.POST.iteritems():
        new_complaint.fill_set.create(field_name=key, val=value)
    new_complaint.save()
    return render(request, 'forms/show.html', {'form': form, 'fields': form.field_set.all()})