# -*- coding: UTF-8 -*-

from django.test import TestCase
import unittest
from forms.utils import *
from django.test.client import RequestFactory
from mock import patch, Mock, MagicMock
from forms.models import Field
from institutions.models import Institution
from complaints.models import File
from users.models import User
from django.contrib.auth.models import User as DjangoUser
import django.core.files as djangoFile
import __builtin__
import myapp.settings as settings

class test_views(TestCase):

    def setUp(self):
        self.request_factory = RequestFactory()
        self.mock_django_user = DjangoUser.objects.create_user('test_user', 'test@testmail.com', 'test_password')
        self.mock_user = User.objects.create(user=self.mock_django_user, permission_level=2)
        inst = Institution(user=self.mock_user, status=2, name='my_inst_name')
        inst.save()
        self.test_form = Form(institution=inst, name='my_form_name')
        self.test_form.save()

    @patch('forms.utils.os.urandom')
    def test_gen_hash(self, mock_rand):
        mock_rand.side_effect = ['test1', 'test2', 'test3']

        self.assertEqual(gen_hash(), b'7465737431')
        self.assertEqual(gen_hash(), b'7465737432')
        mock_rand.assert_called_with(10)

    def test_build_form_query_result_inst_search_existing(self):
        result = build_form_query_result('my_inst')
        self.assertFalse(self._is_empty(result))
        result_form = result.pop()

        self.assertIsInstance(result_form, Form)
        self.assertEqual(result_form.name, 'my_form_name')

    def test_build_form_query_result_form_search_not_existing(self):
        result = build_form_query_result('missing-search')
        self.assertTrue(self._is_empty(result))

    def test_build_form_query_result_form_search(self):
        result = build_form_query_result('my_form')
        self.assertFalse(self._is_empty(result))
        result_form = result.pop()

        self.assertIsInstance(result_form, Form)
        self.assertEqual(result_form.name, 'my_form_name')

    def get_path_user(self):
        mock_user = Mock()
        mock_user.user = self.mock_user
        self.assertEqual(get_path(mock_user, 2), 'user_uploads/1/2')

    def get_path_no_user(self):
        mock_user = Mock
        mock_user.return_value = False
        self.assertEqual(get_path(mock_user, 2), 'user_uploads/anonymous/2')

    @patch('__builtin__.open')
    @patch('forms.utils.os')
    def test_write_to_destination(self, mock_os, mock_open):
        url = '/fake/url'
        mock_file = MagicMock(spec=djangoFile.File)
        mock_file.name = 'docfile'
        mock_file.contents = 'wtf'
        mock_os.path.exists.return_value = False
        mock_os.makedirs.return_value = True
        mock_open.return_value = mock_file

        write_to_destination(url, mock_file)

        mock_os.path.exists.assert_called_with(url)
        mock_os.makedirs.assert_called_with('/'.join(url.split('/')[0:-1]))
        mock_open.assert_called_with(url, 'wb+')
        mock_file.close.assert_any_call()

    '''
    @patch('forms.utils.write_to_destination')
    def test_handle_files(self, mock_write_to_dest):
        hash_val = gen_hash()
        complaint = Complaint(id=1, form = self.test_form, status=0, hash_value = hash_val)
        complaint.save()

        mock_write_to_dest.return_value = True
        mock_file = MagicMock(spec=djangoFile.File)
        mock_file.name = 'mock_file_name'
        files = {'mock_key':mock_file}
        dest_url = settings.MEDIA_ROOT + '/' + get_path(self.mock_user, complaint) + 'mock_file_name'

        handle_files(complaint, files, self.mock_user)

        mock_write_to_dest.assert_called_with(dest_url, mock_file)
    '''

    @patch('forms.utils.handle_files')
    def test_create_complaint(self, mock_handle_files):
        request = self.request_factory.get('', data={})

        field = Field(form=self.test_form, type=1, html_name='fake_html')
        field.save()

        mock_handle_files.return_value = True
        mock_file = MagicMock(spec=djangoFile)

        complaint = create_complaint(self.test_form, self.mock_user, request.GET, mock_file)

        mock_handle_files.assert_called_with(complaint, mock_file, self.mock_user)
        self.assertEqual(complaint.status, 0)
        self.assertEqual(complaint.id, 1)



    def _is_empty(self, struct):
        if struct:
            return False
        return True
