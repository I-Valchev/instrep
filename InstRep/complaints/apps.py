# -*- coding: utf-8 -*-
from django.apps import AppConfig

class ComplaintConfig(AppConfig):
    name = 'complaints'
    verbose_name = u'Оплаквания'
