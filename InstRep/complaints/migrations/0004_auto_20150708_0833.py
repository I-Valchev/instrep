# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('complaints', '0003_auto_20150707_1056'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Complaints',
            new_name='Complaint',
        ),
        migrations.RenameModel(
            old_name='Fills',
            new_name='Fill',
        ),
    ]
