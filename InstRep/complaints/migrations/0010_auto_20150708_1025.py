# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('complaints', '0009_complaint_fill'),
    ]

    operations = [
        migrations.RenameField(
            model_name='complaint',
            old_name='form',
            new_name='form_id',
        ),
    ]
