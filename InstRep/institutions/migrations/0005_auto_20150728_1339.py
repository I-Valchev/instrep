# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('institutions', '0004_institution_upload'),
    ]

    operations = [
        migrations.AlterField(
            model_name='institution',
            name='upload',
            field=models.FileField(upload_to=b'docs'),
        ),
    ]
