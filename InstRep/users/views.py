# -*- coding: utf-8 -*-

from django.contrib import messages
from django.contrib.auth.decorators import user_passes_test
from django.shortcuts import render
from django.shortcuts import redirect
from complaints.models import Complaint
from users.models import User
from myapp.utils import validate_request
from .utils import update_profile

def is_regular_user(user):
	return user.is_authenticated() and User.objects.get(user=user).get_permission_level() == 'regular'

@user_passes_test(is_regular_user, login_url='/accounts/login/')
def profile(request):
	user = User.objects.get(user=request.user)
	context_data = {'user': request.user, 'cur_user': user, 'params': [user]}
	return render(request, 'users/profile.html', context_data)

def submit(request, user_id):
	validate_request(request, 'GETPOST')
	result = update_profile(request, user_id, request.user, request.POST)
	if result['status'] == 'success':
		messages.success(request, 'Профилът е обновен успешно!')
	else:
		messages.error(request, 'Грешна парола!')
	user = User.objects.get(pk=user_id)
	context_data = {'user': request.user, 'cur_user': user, 'params': [request.POST,request.user]}
	return render(request, 'users/profile.html', context_data)

@user_passes_test(is_regular_user, login_url='/accounts/login/')
def dashboard(request):
	validate_request(request, 'GETPOST')
	user = User.objects.get(user=request.user)
	complaints = Complaint.objects.filter(user=user)

	return render(request, 'users/profile_dashboard.html', {'complaints': complaints})
