# -*- coding: UTF-8 -*-

from django.test import TestCase
from complaints.admin import *
from django.test.client import RequestFactory
from django.contrib.admin.sites import AdminSite
from users.models import User
from forms.models import Form
from django.contrib.auth.models import User as DjangoUser
from institutions.models import Institution
from django.http.response import Http404
from django.test.client import Client
from mock import patch

class test_complaint_admin(TestCase):
    def setUp(self):
        self.request_factory = RequestFactory()
        mock_django_user = DjangoUser.objects.create_user(username='test_user',
            email='test@testmail.com', password='test_password')
        self.mock_user = User.objects.create(user=mock_django_user, permission_level=2)
        self.mock_user.save()
        self.inst = Institution(name='TestInst', user=self.mock_user, status=2)
        self.inst.save()
        self.form = Form(institution=self.inst, name='fake_form')
        self.form.save()
        site = AdminSite()
        self.complaint = Complaint(form=self.form, status=1)
        self.complaint.save()
        self.complaint_admin = ComplaintAdmin(self.complaint, site)

    def test_get_queryset_fake_request(self):
        request = 'not request'
        self.assertRaises(Http404, self.complaint_admin.get_queryset, request)
        
    def test_get_queryset_superuser(self):
        request = self.request_factory.get('', data={})
        request.user = self.mock_user
        request.user.is_superuser = True

        result_complaint = self.complaint_admin.get_queryset(request)[0]
        self.assertEqual(result_complaint.status, 1)

    @patch('users.models.User.objects.get')
    def test_get_queryset_no_superuser(self, mock_get_user):
        request = self.request_factory.get('', data={})
        request.user = self.mock_user
        request.user.is_superuser = False
        mock_get_user.return_value = self.mock_user

        result_complaint = self.complaint_admin.get_queryset(request)[0]
        self.assertEqual(result_complaint.status, 1)
        mock_get_user.assert_called_with(user=request.user)

    def test_get_readonly_fields_fake_request(self):
        request = 'faker'
        self.assertRaises(Http404, self.complaint_admin.get_readonly_fields, request)

    def test_get_readonly_fields_no_superuser(self):
        request = self.request_factory.get('', data={})
        request.user = self.mock_user
        request.user.is_superuser = False

        result = self.complaint_admin.get_readonly_fields(request)
        self.assertEqual(result, ('user', 'form', 'hash_value'))

    def test_get_readonly_fields_superuser(self):
        request = self.request_factory.get('', data={})
        request.user = self.mock_user
        request.user.is_superuser = True

        result = self.complaint_admin.get_readonly_fields(request, self.form)
        self.assertTrue(not result)

    def test_get_actions_fake_request(self):
        request = 'not request'
        self.assertRaises(Http404, self.complaint_admin.get_actions, request)

    def test_get_actions_no_superuser(self):
        request = self.request_factory.get('', data={})
        request.user = self.mock_user
        request.user.is_superuser = False

        result = self.complaint_admin.get_actions(request)
        self.assertFalse(self._delete_selected(result))

    def test_get_actions_superuser(self):
        request = self.request_factory.get('', data={})
        request.user = self.mock_user
        request.user.is_superuser = True

        result = self.complaint_admin.get_actions(request)
        self.assertTrue(self._delete_selected(result))

    def _delete_selected(self, struct):
        if 'delete_selected' in struct:
            return True
        return False
