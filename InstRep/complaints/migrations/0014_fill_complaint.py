# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('complaints', '0013_auto_20150708_1027'),
    ]

    operations = [
        migrations.AddField(
            model_name='fill',
            name='complaint',
            field=models.ForeignKey(default=-1, to='complaints.Complaint'),
            preserve_default=False,
        ),
    ]
