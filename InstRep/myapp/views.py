# -*- coding: utf-8 -*-
from django.contrib import messages
from django.shortcuts import render
from django.shortcuts import redirect

from institutions.forms import InstitutionProfileForm
from institutions.models import Institution
from users.models import User

def home(request):
	if request.user.is_authenticated():
		if request.user.is_superuser:
			return redirect('/admin')

		user = User.objects.get(user=request.user)
		if user.get_permission_level() != 'regular':
			return redirect('/admin')

	return render(request, 'home.html')

def inst_profile(request):
	user = User.objects.get(user=request.user)
	institution = Institution.objects.get(user=user)

	if request.method == 'POST':
		form = InstitutionProfileForm(request.POST)
		if form.is_valid():
			institution.name = form.cleaned_data['inst_name']
			institution.user.city = form.cleaned_data['city']
			institution.user.phone = form.cleaned_data['phone']
			institution.notified = form.cleaned_data['notified']
			institution.save()

			messages.success(request, 'Профилът е обновен успешно!')
			return redirect('/')
	else:
		form = InstitutionProfileForm(initial={
			'inst_name': institution.name, 
			'city': institution.user.city,
			'phone': institution.user.phone,
			'notified': institution.notified
		})

	return render(request, 'admin/inst_profile.html', {'form': form})
