# -*- coding: UTF-8 -*-

from django.test import TestCase
from auth.views import *
from mock import patch, Mock
from django.test.client import RequestFactory
from django.contrib.sessions.middleware import SessionMiddleware

class test_views(TestCase):

	def setUp(self):
		self.request_factory = RequestFactory()

	@patch('auth.views.auto_login')
	@patch('auth.views.messages')
	@patch('auth.views.is_not_logged_in')
	def test_signup_successful(self, mock_is_not_logged, mock_messages, mock_auto_login):
		mock_is_not_logged.return_value = True
		mock_auto_login.return_value = True
		mock_user = Mock()
		mock_user.is_authenticated.return_value = False
		mock_user.is_active.return_value = True
		request = self.request_factory.post('', data={'email': u'signup@mail.com', 'pword':u'signup_password',
			'remember_me':None})
		request.user = mock_user

		response = signup(request)

		mock_is_not_logged.assert_called_with(mock_user)
		mock_messages.success.assert_called_with(request, 'Успешно влизане в профила!')
		self.assertEqual(response, True)

	@patch('auth.views.messages')
	@patch('auth.views.authenticate')
	def test_login_valid_and_authenticated(self, mock_auth, mock_messages):
		request = self.request_factory.post('', data={'email': u'test@abv.bg', 'password': u'PA$$WORD'})

	   	user = User.objects.create_user('test_user', 'test@testmail.com', 'test_password')
	   	user.backend='django.contrib.auth.backends.ModelBackend'
		user.is_active = False

		mock_auth.return_value = user
		request.user = user
		login(request)

		mock_messages.error.assert_called_with(request, 'Вече сте в системата!')


	@patch('auth.views.auto_login')
	@patch('auth.views.messages')
	@patch('auth.views.authenticate')
	@patch('auth.views.is_not_logged_in')
	def test_login_successful(self, mock_is_not_logged, mock_auth, mock_messages, mock_auto_login):
		request = self.request_factory.post('', data={})
		mock_user = Mock()
		mock_user.is_active = True
		mock_user.is_authenticated = False
		request.user = mock_user

		middleware = SessionMiddleware()
	   	middleware.process_request(request)
	   	request.session.save()

		mock_auth.return_value = mock_user
		mock_auto_login.return_value = True
		mock_is_not_logged.return_value = True
		response = login(request)

		self.assertEqual(response, True)
		mock_messages.success(request, 'Успешно влизане в профила!')
		mock_auto_login.assert_called_with(request, mock_user)

	def test_login_get(self):
	   	user = User.objects.create_user('test_user', 'test@testmail.com', 'test_password')
		response = self.client.get('/accounts/login/', {'user':user})
		self.assertTemplateUsed(response, 'auth/login.html')

	@patch('auth.views.auto_login')
	@patch('auth.views.create_institution')
	@patch('auth.views.user_exists')
	@patch('auth.views.is_not_logged_in')
	def test_institution_signup(self, mock_is_not_logged, mock_user_exists, mock_create_institution,
		mock_auto_login):

		request = self.request_factory.post('', data={'email':'not_taken'})
		mock_is_not_logged.return_value = True
		mock_user_exists.return_value = False
		mock_user = Mock()
		request.user = mock_user
		mock_auto_login.return_value = True

		self.assertEqual(institution_signup(request), True)
		mock_is_not_logged.assert_called_with(mock_user)
		mock_create_institution.assert_called_with(request.POST)
