# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('forms', '0034_auto_20150911_2059'),
    ]

    operations = [
        migrations.AddField(
            model_name='field',
            name='choose_one',
            field=models.PositiveSmallIntegerField(default=-1),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='field',
            name='maximum',
            field=models.IntegerField(default=-1),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='field',
            name='minimum',
            field=models.IntegerField(default=-1),
            preserve_default=False,
        ),
    ]
