# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('institutions', '0002_institution_user'),
    ]

    operations = [
        migrations.AlterField(
            model_name='institution',
            name='user',
            field=models.ForeignKey(to='users.User'),
        ),
    ]
