# -*- coding: UTF-8 -*-

from django.test import TestCase
from forms.admin import *
from django.test.client import RequestFactory
from django.contrib.admin.sites import AdminSite
from users.models import User
from django.contrib.auth.models import User as DjangoUser
from institutions.models import Institution
from django.http.response import Http404
from django.test.client import Client
from mock import patch

class test_admin(TestCase):
    def setUp(self):
        self.request_factory = RequestFactory()
        mock_django_user = DjangoUser.objects.create_user(username='test_user',
            email='test@testmail.com', password='test_password')
        self.mock_user = User.objects.create(user=mock_django_user, permission_level=2)
        self.mock_user.save()
        self.inst = Institution(name='TestInst', user=self.mock_user, status=2)
        self.inst.save()
        self.form = Form(institution=self.inst, name='fake_form')
        self.form.save()
        site = AdminSite()
        self.form_admin = FormAdmin(self.form, site)
        self.field = Field(form=self.form, name='fake_field', type=1)
        self.field.save()

    def test_approve_form_fake_request(self):
        request = 'fake'
        queryset = Form.objects.all()
        self.assertRaises(Http404, self.form_admin.approve_form, request, queryset)

    def test_approve_form(self):
        self.form.approved = False
        request = self.request_factory.post('', data={})
        queryset = Form.objects.all()

        self.form_admin.approve_form(request, queryset)

        self.assertTrue(queryset[0].approved)


    def test_main_field_select_view_fake_request(self):
        request = 123
        form_id = 1

        self.assertRaises(Http404, self.form_admin.main_field_select_view, request, form_id)

    @patch('django.contrib.messages.success')
    def test_main_field_select_view_post_request(self, mock_add):
        mock_add.return_value = True
        request = self.request_factory.post('', data={'main_field_name':'name'})

        r = self.form_admin.main_field_select_view(request, 1)
        self.form = Form.objects.get(pk=1) #aka update the form

        self.assertEqual(self.form.main_field, 'name')
        self.assertEqual(r.url, '/admin/forms')

    def test_get_actions_fake_request(self):
        request = 'fake request'

        self.assertRaises(Http404, self.form_admin.get_actions, request)

    def test_get_actions_no_superuser(self):
        request = self.request_factory.get('', data={})
        request.user = self.mock_user
        request.user.is_superuser = False

        actions = self.form_admin.get_actions(request)

        self.assertFalse(self._has_approve_form(actions))

    def test_get_actions(self):
        request = self.request_factory.get('', data={})
        request.user = self.mock_user
        request.user.is_superuser = True

        actions = self.form_admin.get_actions(request)

        self.assertTrue(self._has_approve_form(actions))
        self.assertEqual(actions['approve_form'][2], u'\u041e\u0434\u043e\u0431\u0440\u0438 \u0444\u043e\u0440\u043c\u0430\u0442\u0430')

    def test_get_fields_fake_request(self):
        request = 'fake'
        self.assertRaises(Http404, self.form_admin.get_fields, request)

    def test_get_fields_no_superuser(self):
        request = self.request_factory.get('', data={})
        request.user = self.mock_user
        request.user.is_superuser = False

        self.assertEqual(self.form_admin.get_fields(request), ('name',))

    def test_response_add(self):
        request = self.request_factory.post('', data={})
        r = self.form_admin.response_add(request, self.form)
        self.assertEqual(r.url, '/admin/forms/form/1/main_field_select_view')

    def test_response_change(self):
        request = self.request_factory.post('', data={})
        r = self.form_admin.response_change(request, self.form)
        self.assertEqual(r.url, '/admin/forms/form/1/main_field_select_view')

    def test_save_model_fake_request(self):
        request = 'no request'
        self.assertRaises(Http404, self.form_admin.save_model,request, 1,1,1)

        request = self.request_factory.get('', data={})
        self.assertRaises(Http404, self.form_admin.save_model,request, 1,1,1)

    @patch('users.models.User.objects.get')
    @patch('institutions.models.Institution.objects.get')
    @patch('forms.models.Form.save')
    def test_save_model_no_superuser(self, mock_save, mock_get_inst, mock_get_user):
        request = self.request_factory.post('', data={})
        request.user = self.mock_user
        request.user.is_superuser = False

        mock_get_inst.return_value = self.inst
        mock_get_user.return_value = self.mock_user

        self.form_admin.save_model(request, self.form, self.form, 1)

        #any_call means called without args
        mock_get_user.assert_called_with(user=self.mock_user)
        mock_get_inst.assert_called_with(user=self.mock_user)
        mock_save.assert_any_call()
        self.assertEqual(self.form.institution.id, 1)

    @patch('forms.models.Form.save')
    def test_save_model(self, mock_save):
        request = self.request_factory.post('', data={})
        request.user = self.mock_user
        request.user.is_superuser = True

        self.form_admin.save_model(request, self.form, self.form, 1)

        mock_save.assert_any_call()

    def test_get_queryset_fake_request(self):
        request = 123
        self.assertRaises(Http404, self.form_admin.get_queryset, request)

    def test_get_queryset_superuser(self):
        request = self.request_factory.get('', data={})
        request.user = self.mock_user
        request.user.is_superuser = True

        result_form = self.form_admin.get_queryset(request)[0]
        self.assertEqual(result_form.name, 'fake_form')

    @patch('users.models.User.objects.get')
    def test_get_queryset_no_superuser(self, mock_get_user):
        request = self.request_factory.get('', data={})
        request.user = self.mock_user
        request.user.is_superuser = False
        mock_get_user.return_value = self.mock_user

        result_form = self.form_admin.get_queryset(request)[0]
        self.assertEqual(result_form.name, 'fake_form')
        mock_get_user.assert_called_with(user=request.user)

    def test_get_readonly_fields_fake_request(self):
        request = 'faker'
        self.assertRaises(Http404, self.form_admin.get_readonly_fields, request)

    def test_get_readonly_fields_no_superuser(self):
        request = self.request_factory.get('', data={})
        request.user = self.mock_user
        request.user.is_superuser = False

        result = self.form_admin.get_readonly_fields(request)
        self.assertEqual(result, ('institution', 'main_field'))

    def test_get_readonly_fields_superuser(self):
        request = self.request_factory.get('', data={})
        request.user = self.mock_user
        request.user.is_superuser = True

        result = self.form_admin.get_readonly_fields(request, self.form)
        self.assertTrue(not result)


    def _has_approve_form(self, struct):
        if 'approve_form' in struct:
            return True
        return False
