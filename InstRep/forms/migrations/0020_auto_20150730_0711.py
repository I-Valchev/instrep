# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('forms', '0019_auto_20150729_1222'),
    ]

    operations = [
        migrations.AlterField(
            model_name='form',
            name='main_field',
            field=models.CharField(max_length=200),
        ),
    ]
