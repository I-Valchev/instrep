# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='permission_level',
            field=models.PositiveSmallIntegerField(choices=[(1, b'regular'), (2, b'institution'), (3, b'superadmin')]),
        ),
    ]
