# -*- coding: UTF-8 -*-

from django.test import TestCase
from institutions.views import *
from django.http.response import Http404, HttpResponse
from django.test.client import RequestFactory
from users.models import User
from django.contrib.auth.models import User as DjangoUser
import django.core.files as djangoFile
from mock import patch, Mock
from institutions import models
from forms.models import Form
from forms.utils import gen_hash
from complaints.models import Complaint
  
class test_views(TestCase):
    def setUp(self):
        self.request_factory = RequestFactory()

        self.mock_django_user = DjangoUser.objects.create_user('test_user', 'test@testmail.com', 'test_password')
        mock_user = User.objects.create(user=self.mock_django_user, permission_level=2)
        self.inst = Institution(name='TestInst', user=mock_user, status=1)
        self.inst.save()

        self.mock_django_user2 = DjangoUser.objects.create_user('test_user2', 'test2@testmail.com', 'test_password2')
        mock_user2 = User.objects.create(user=self.mock_django_user2, permission_level=2)
        self.inst2 = Institution(name='SecondTestInst', user=mock_user2, status=2)
        self.inst2.save()        

    def test_upload_wrong_data(self):
        request = '123'
        self.assertRaises(Http404, upload, request, 1)

    @patch('institutions.models.Institution.save')
    @patch('institutions.forms.DocumentForm.is_valid')
    def test_upload_real_data(self, mock_validation, mock_save):
        mock_file = Mock(spec=djangoFile.File)
        mock_file.name = 'docfile'
        request = self.request_factory.post('', data={'docfile':mock_file})
        mock_validation.return_value = True
        mock_save.return_value = True
        self.assertIsInstance(upload(request, 1), HttpResponse)
        
        get_request = self.request_factory.get('', data={})
        self.assertIsInstance(upload(request, 1), HttpResponse)
        
        response = self.client.get('/upload/1/')
        self.assertTemplateUsed(response, 'institutions/upload.html')

    def test_upload_no_such_institution(self):
        request = self.request_factory.post('', data={})
        self.assertRaises(Http404, upload, request, 10)
        
    def test_thanks_for_submission_wrong_data(self):
        request = 123
        self.assertRaises(Http404, thanks_for_submission, request)
        
        request = self.request_factory.post('', data={})
        self.assertRaises(Http404, thanks_for_submission, request)
        
    def test_thanks_for_submission_real_data(self):
        response = self.client.get('/thanks_for_submission/')
        
        self.assertTemplateUsed(response, 'institutions/thanks_for_submission.html')
        
        
    def test_leaderboard_fake_request(self):
        request = 'wrong'
        self.assertRaises(Http404, leaderboard, request)

        request = self.request_factory.post('', data={})
        self.assertRaises(Http404, leaderboard, request)

    def test_leaderboard_real_data(self):

        self.inst.status = 2
        self.inst.save()

        request = self.request_factory.get('', data={})
        self.assertIsInstance(leaderboard(request), HttpResponse)


        test_form = Form(institution=self.inst)
        test_form.save()
        hash_val = gen_hash()
        complaint = Complaint(id=1, form = test_form, status=0, hash_value = hash_val)
        complaint.save()

        response = self.client.get('/institutions/leaderboard/')
        self.assertTemplateUsed(response, 'institutions/leaderboard.html')
        self.assertTrue(response.context['most_active_institutions'][0], 'TestInst')
        self.assertTrue(response.context['most_resolving_institutions'][0], 'TestInst')