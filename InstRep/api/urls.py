from rest_framework import routers
from django.conf.urls import include, url
from forms.viewset import FormViewSet
from complaints.viewset import ComplaintViewSet
from users.viewset import UserViewSet
from sign_up import sign_up
from form_submit import form_submit
from institution_sign_up import institution_sign_up
from get_file import get_file

router = routers.DefaultRouter()
router.register(r'forms', FormViewSet, 'forms')
router.register(r'complaints', ComplaintViewSet, 'complaints')
router.register(r'users', UserViewSet, 'users')

urlpatterns = [
    url(r'^build_form', 'api.views.build_form'),
    url(r'^sign_up/', sign_up),
    url(r'^form_submit/(?P<form_id>[0-9]+)/', form_submit),
    url(r'^institution_sign_up/', institution_sign_up),
    url(r'^get_file/', get_file),
    url(r'^', include(router.urls)),
]