# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('forms', '0007_field_required'),
    ]

    operations = [
        migrations.RenameField(
            model_name='fieldoption',
            old_name='field_id',
            new_name='field',
        ),
    ]
