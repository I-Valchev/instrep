# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('forms', '0020_auto_20150730_0711'),
    ]

    operations = [
        migrations.AddField(
            model_name='field',
            name='autofill',
            field=models.PositiveSmallIntegerField(default=-1, choices=[(0, b'first_name'), (1, b'last_name'), (2, b'city'), (3, b'phone'), (4, b'email'), (5, b'address')]),
            preserve_default=False,
        ),
    ]
