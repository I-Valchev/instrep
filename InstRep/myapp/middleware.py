from auth.utils import redirect_accordingly
from django.conf import settings
from django.contrib.messages import get_messages
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.utils import translation
from institutions.models import Institution
from users.models import User

import re

class RestrictAdminMiddleware(object):
	def process_request(self, request):
		trying_to_access_admin = re.match(r'/admin', request.path)
		if trying_to_access_admin:
			redirect = redirect_accordingly(request.user)
			if redirect['location'] != '/admin/':
				return redirect

class AdminLocaleURLMiddleware:
	def process_request(self, request):
		if request.path.startswith('/admin'):
			request.LANG = getattr(settings, 'ADMIN_LANGUAGE_CODE', settings.LANGUAGE_CODE)
			translation.activate(request.LANG)
			request.LANGUAGE_CODE = request.LANG