# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('forms', '0016_auto_20150723_1136'),
        ('forms', '0016_merge'),
    ]

    operations = [
    ]
