# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('forms', '0026_auto_20150810_2202'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='field',
            options={'verbose_name': '\u043f\u043e\u043b\u0435', 'verbose_name_plural': '\u043f\u043e\u043b\u0435\u0442\u0430'},
        ),
        migrations.AlterModelOptions(
            name='fieldoption',
            options={'verbose_name': '\u043e\u043f\u0446\u0438\u044f', 'verbose_name_plural': '\u043e\u043f\u0446\u0438\u0438'},
        ),
        migrations.AlterModelOptions(
            name='form',
            options={'verbose_name': '\u0444\u043e\u0440\u043c\u0430', 'verbose_name_plural': '\u0444\u043e\u0440\u043c\u0438'},
        ),
        migrations.AddField(
            model_name='field',
            name='validation_type',
            field=models.PositiveSmallIntegerField(default=-1, verbose_name=b'\xd0\xa2\xd0\xb8\xd0\xbf \xd0\xbd\xd0\xb0 \xd0\xb2\xd0\xb0\xd0\xbb\xd0\xb8\xd0\xb4\xd0\xb0\xd1\x86\xd0\xb8\xd1\x8f\xd1\x82\xd0\xb0', choices=[(-1, b'-'), (0, b'\xd0\xa2\xd0\xb5\xd0\xbb\xd0\xb5\xd1\x84\xd0\xbe\xd0\xbd'), (1, b'\xd0\x9f\xd0\xbe\xd1\x89\xd0\xb5\xd0\xbd\xd1\x81\xd0\xba\xd0\xb8 \xd0\xba\xd0\xbe\xd0\xb4'), (2, b'\xd0\x98\xd0\xbc\xd0\xb5')]),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='form',
            name='main_field',
            field=models.CharField(max_length=200, null=True, blank=True),
        ),
    ]
