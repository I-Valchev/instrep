# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('complaints', '0015_complaint_status'),
    ]

    operations = [
        migrations.AlterField(
            model_name='complaint',
            name='status',
            field=models.PositiveSmallIntegerField(choices=[(0, b'Pending'), (1, b'In revision'), (2, b'Will not be adressed'), (3, b'Solved')]),
        ),
    ]
