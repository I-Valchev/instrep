# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('complaints', '0028_comment'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='complaint',
            options={'verbose_name': '\u041e\u043f\u043b\u0430\u043a\u0432\u0430\u043d\u0435', 'verbose_name_plural': '\u041e\u043f\u043b\u0430\u043a\u0432\u0430\u043d\u0438\u044f'},
        ),
        migrations.AlterModelOptions(
            name='fill',
            options={'verbose_name': '\u041f\u043e\u043b\u0435', 'verbose_name_plural': '\u041f\u043e\u043b\u0435\u0442\u0430'},
        ),
        migrations.AlterField(
            model_name='complaint',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, verbose_name=b'\xd0\x94\xd0\xb0\xd1\x82\xd0\xb0'),
        ),
        migrations.AlterField(
            model_name='complaint',
            name='hash_value',
            field=models.CharField(max_length=20, verbose_name=b'\xd0\xa5\xd0\xb5\xd1\x88\xd0\xb8\xd1\x80\xd0\xb0\xd0\xbd \xd0\xba\xd0\xbe\xd0\xb4'),
        ),
        migrations.AlterField(
            model_name='complaint',
            name='status',
            field=models.PositiveSmallIntegerField(verbose_name=b'\xd0\xa1\xd1\x82\xd0\xb0\xd1\x82\xd1\x83\xd1\x81', choices=[(0, b'\xd0\x9e\xd0\xb1\xd1\x80\xd0\xb0\xd0\xb1\xd0\xbe\xd1\x82\xd0\xb2\xd0\xb0 \xd1\x81\xd0\xb5'), (1, b'\xd0\x9e\xd1\x82\xd1\x85\xd0\xb2\xd1\x8a\xd1\x80\xd0\xbb\xd0\xb5\xd0\xbd\xd0\xb0'), (2, b'\xd0\xa0\xd0\xb0\xd0\xb7\xd1\x80\xd0\xb5\xd1\x88\xd0\xb5\xd0\xbd\xd0\xb0')]),
        ),
        migrations.AlterField(
            model_name='complaint',
            name='user',
            field=models.ForeignKey(verbose_name=b'\xd0\x9f\xd0\xbe\xd1\x82\xd1\x80\xd0\xb5\xd0\xb1\xd0\xb8\xd1\x82\xd0\xb5\xd0\xbb', to='users.User', null=True),
        ),
        migrations.AlterField(
            model_name='fill',
            name='field_name',
            field=models.CharField(max_length=100, verbose_name=b'\xd0\x98\xd0\xbc\xd0\xb5 \xd0\xbd\xd0\xb0 \xd0\xbf\xd0\xbe\xd0\xbb\xd0\xb5\xd1\x82\xd0\xbe'),
        ),
        migrations.AlterField(
            model_name='fill',
            name='val',
            field=models.CharField(max_length=200, verbose_name=b'\xd0\xa1\xd1\x82\xd0\xbe\xd0\xb9\xd0\xbd\xd0\xbe\xd1\x81\xd1\x82 \xd0\xbd\xd0\xb0 \xd0\xbf\xd0\xbe\xd0\xbb\xd0\xb5\xd1\x82\xd0\xbe'),
        ),
    ]
