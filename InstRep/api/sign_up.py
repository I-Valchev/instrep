# -*- coding: utf-8 -*-

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import authentication, permissions

from rest_framework.decorators import api_view, permission_classes
from rest_framework import viewsets, permissions

from auth.utils import create_user

@api_view(['GET', 'POST'])
@permission_classes([permissions.IsAuthenticated])
def sign_up(request):
    email = username = request.POST.get('email')
    password = request.POST.get('password')

    user = create_user(username, email, password)
    if user == 'email_taken':
        status = user
    else:
        status = 'success'

    return Response({'status': status})