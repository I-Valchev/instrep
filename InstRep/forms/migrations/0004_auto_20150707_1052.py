# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('forms', '0003_fieldoptions'),
    ]

    operations = [
        migrations.AddField(
            model_name='field',
            name='form_id',
            field=models.ForeignKey(default=-1, to='forms.Form'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='field',
            name='name',
            field=models.CharField(default=-1, max_length=20),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='field',
            name='type',
            field=models.CharField(default=-1, max_length=20),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='fieldoptions',
            name='field_id',
            field=models.ForeignKey(default=-1, to='forms.Field'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='fieldoptions',
            name='option',
            field=models.CharField(default=-1, max_length=50),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='form',
            name='inst_name',
            field=models.CharField(default=-1, max_length=200),
            preserve_default=False,
        ),
    ]
