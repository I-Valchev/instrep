from django.test import TestCase
from complaints.admin import *
from django.http.response import Http404
from django.test.client import RequestFactory
from complaints.models import Complaint
from forms.models import Form
from institutions.models import Institution
from django.contrib.auth.models import User as DjangoUser
from users.models import User
from forms.utils import gen_hash
from django.contrib.admin.sites import AdminSite

class test_admin_comment(TestCase):

	def setUp(self):
		self.request_factory = RequestFactory()

		mock_django_user = DjangoUser.objects.create_user('test_user', 'test@testmail.com', 'test_password')
		self.mock_user = User.objects.create(user=mock_django_user, permission_level=2)
		inst = Institution(user=self.mock_user, status=2, name='fake_inst_name')
		inst.save()
		test_form = Form(institution=inst)
		test_form.save()
		hash_val = gen_hash()
		self.complaint = Complaint(id=1, form = test_form, status=0, hash_value = hash_val,)
		self.complaint.save()
		self.comment = Comment(complaint=self.complaint)
		site = AdminSite()
		self.comment_view_admin = CommentViewInline(self.comment, site)
		self.comment_add_admin = CommentAddInline(self.comment, site)

	def test_comment_view_get_readonly_fields_fake_request(self):
		request = 'faker'
		self.assertRaises(Http404, self.comment_view_admin.get_readonly_fields, request)

	def test_comment_view_get_readonly_fields(self):
		request = self.request_factory.get('', data={})

		result = self.comment_view_admin.get_readonly_fields(request)
		self.assertEqual(result, ('text',))

	def test_comment_view_has_add_permission_fake_request(self):
		request = 'fake_request'
		self.assertRaises(Http404, self.comment_view_admin.has_add_permission, request)

	def test_comment_view_has_add_permission(self):
		request = self.request_factory.get('', data={})
		self.assertFalse(self.comment_view_admin.has_add_permission(request))

	def test_comment_add_has_change_permission_fake_request(self):
		request = 'fake_request'
		self.assertRaises(Http404, self.comment_add_admin.has_change_permission, request)

	def test_comment_add_has_change_permission(self):
		request = self.request_factory.get('', data={})
		self.assertFalse(self.comment_add_admin.has_change_permission(request))
