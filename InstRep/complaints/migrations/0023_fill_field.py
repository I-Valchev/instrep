# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('forms', '0020_auto_20150730_0711'),
        ('complaints', '0022_auto_20150727_1227'),
    ]

    operations = [
        migrations.AddField(
            model_name='fill',
            name='field',
            field=models.ForeignKey(default=0, to='forms.Field'),
            preserve_default=False,
        ),
    ]
