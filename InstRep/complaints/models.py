# -*- coding: utf-8 -*-
from django.db import models
from users.models import User 
from forms.models import Form, Field 
import datetime

COMPLAINT_STATUSES = (
    (0, "Обработва се"),
    (1, "Отхвърлена"),
    (2, "Разрешена")
)

class Complaint(models.Model):
    form = models.ForeignKey(Form, on_delete=models.SET_NULL, null=True, verbose_name="Форма")
    status = models.PositiveSmallIntegerField(choices=COMPLAINT_STATUSES, verbose_name="Статус")
    created_at = models.DateTimeField(auto_now_add=True, verbose_name="Дата")
    updated_at = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(User, null=True, verbose_name="Потребител")
    hash_value = models.CharField(max_length=20, verbose_name="Хеширан код")
    
    def __unicode__(self):
        if self.form:
            return u'Оплакване до %s' % self.form.institution.name
        else:
            return u'Оплакване до изтрита форма. :('

    class Meta:
        verbose_name = 'оплакване'
        verbose_name_plural = 'оплаквания'
    
from django.core.exceptions import ValidationError
class Fill(models.Model):
    complaint = models.ForeignKey(Complaint)
    field_name = models.CharField(max_length=200, verbose_name="Име на полето")
    val = models.CharField(max_length = 200, verbose_name="Стойност на полето", null=True, blank=True)

    class Meta:
        verbose_name = u'поле'
        verbose_name_plural = u'полета'

    def clean(self):
        field = self.complaint.form.field_set.get(name=self.field_name)
        if field.type == 1 or field.type == 2:
            minimum = float('-inf') if field.minimum is None or field.minimum == -1 else field.minimum
            maximum = float('inf') if field.maximum is None or field.maximum == -1 else field.maximum
            if field.validation_type == 3: # number
                if int(self.val) > maximum or int(self.val) < minimum:
                    raise ValidationError("Field %s must be a number in the range %f-%f" % (self.field_name, minimum, maximum))
            else: # regular string
                if len(self.val) > maximum or len(self.val) < minimum:
                    raise ValidationError(u"Field {0} must have length in interval {1}-{2}".format(self.field_name, minimum, maximum))

    def save(self, **kwargs):
        self.clean()
        return super(Fill, self).save(**kwargs)

    def __unicode__(self):
        return u'Поле'

def generate_filename(self, filename):
    url = "files/users/%s/%s" % (self.user.username, filename)
    return url
    
class File(models.Model):
    complaint = models.ForeignKey(Complaint)
    field_name = models.CharField(max_length = 100)
    val = models.FileField(upload_to=generate_filename, blank=True, null=True)
    
class Comment(models.Model):
    complaint = models.ForeignKey(Complaint)
    date = models.DateTimeField(auto_now_add=True)
    text = models.TextField()

    class Meta:
        verbose_name = u'коментар'
        verbose_name_plural = u'коментари'
