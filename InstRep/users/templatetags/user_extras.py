from django import template
register = template.Library()

@register.filter
def filter(params, field):
	if len(params) == 2:
		request_params = params[0]
		return request_params[field]
	elif len(params) == 1:
		user = params[0]
		field = eval("user.%s" % (field))
		if field:
			return field
		else:
			return ""
	