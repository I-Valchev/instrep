# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('institutions', '0005_auto_20150728_1339'),
    ]

    operations = [
        migrations.AddField(
            model_name='institution',
            name='status',
            field=models.PositiveSmallIntegerField(default=0, choices=[(b'0', b'inactive'), (b'1', b'pending'), (b'2', b'active')]),
            preserve_default=False,
        ),
    ]
