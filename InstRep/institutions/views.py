from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.http.response import Http404
from django.shortcuts import redirect, render, render_to_response
from django.template import RequestContext

from institutions.models import Institution
from institutions.forms import DocumentForm
from myapp.utils import validate_request

def upload(request, institution_id):
    validate_request(request, 'GETPOST')
    if request.method == 'POST':
        try:
            inst = Institution.objects.get(pk=institution_id)
        except:
            raise Http404
        form = DocumentForm(request.POST, request.FILES)
        if form.is_valid():
            inst.upload = request.FILES['docfile']
            inst.status = 1
            inst.save()
            return redirect('thanks_for_submission')
            
    else:
        form = DocumentForm()
        
    return render_to_response(
        'institutions/upload.html',
        {'form': form, "institution_id": institution_id},
        context_instance=RequestContext(request)
    )

def thanks_for_submission(request):
    validate_request(request, 'GET')
    return render(request, 'institutions/thanks_for_submission.html')

def leaderboard(request):
    validate_request(request, 'GET')
    institutions = Institution.objects.filter(status=2)
    least_active_institutions = sorted(institutions, key = lambda x: x.get_pending_complaints_percentage())
    most_active_institutions = sorted(institutions, key = lambda x: x.get_pending_complaints_percentage(), reverse = True)
    most_resolving_institutions = sorted(institutions, key = lambda x: x.get_resolved_complaints_count(), reverse = True)
    most_rejecting_institutions = sorted(institutions, key = lambda x: x.get_rejected_complaints_count(), reverse = True)
    return render(request, 'institutions/leaderboard.html', 
        {'least_active_institutions': least_active_institutions, 'most_active_institutions': most_active_institutions,
         'most_resolving_institutions': most_resolving_institutions, 'most_rejecting_institutions': most_rejecting_institutions}
    )
