# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import institutions.models


class Migration(migrations.Migration):

    dependencies = [
        ('institutions', '0006_institution_status'),
    ]

    operations = [
        migrations.AlterField(
            model_name='institution',
            name='upload',
            field=models.FileField(upload_to=institutions.models.generate_filename),
        ),
    ]
