# -*- coding: UTF-8 -*-

from django.test import TestCase
from users.utils import *
from users.models import User
from django.contrib.auth.models import User as DjangoUser
from django.test.client import RequestFactory
from mock import patch, Mock

class test_models(TestCase):
    def setUp(self):
        self.request_factory = RequestFactory()
        mock_django_user = DjangoUser.objects.create_user('test_user', 'test@testmail.com', 'test_password')
        self.mock_user = User.objects.create(user=mock_django_user, permission_level=1)

    @patch('django.contrib.auth.models.User.set_password')
    @patch('users.utils.auto_login')
    def test_change_password(self, mock_auto_login, mock_set_password):
        request = self.request_factory.post('', data={})

        change_password(request, self.mock_user, 'my_new_password')

        mock_set_password.assert_called_with('my_new_password')
        mock_auto_login.assert_called_with(request, self.mock_user.user)

    @patch('users.utils.change_password')
    @patch('users.utils.User.objects.get')
    def test_update_profile_successfully(self, mock_get_user, mock_change_password):
        request = self.request_factory.post('', data={})
        data =  {'email':'mock_mail@gmail.com', 'new_pass':'new_mock_pass', 'old_pass':'no_more_pass'}
        mock_user = Mock()
        mock_get_user.return_value = mock_user
        mock_request_user = Mock()
        mock_request_user.check_password.return_value = True
        response = update_profile(request, 1, mock_request_user, data)

        mock_change_password.assert_called_with(request, mock_user, 'new_mock_pass')
        self.assertEqual(response['status'], 'success')
        mock_user.save.assert_any_call()
