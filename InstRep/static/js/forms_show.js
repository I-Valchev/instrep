$(document).ready(function(){
  // Activates tooltip on hover over elements with class data-toggle="tooltip" -->
    $('[data-toggle="tooltip"]').tooltip();

    // If data-minimum or maximum is NULL in the DB, set the respective attribute to -1
    $("input[type=\"text\"][data-minimum=\"None\"]").attr("data-minimum", -1);
    $("input[type=\"text\"][data-maximum=\"None\"]").attr("data-maximum", -1);
});

// Prevents form submission if any front-end validations fail
$(document).ready(function() {
      $("form").submit(function(e){
        var count_bad_validation = $(".has-error").length;

        // Handle "choose_one" option
        var queryset = $("input[type='text'][data-choose-one!=-1], input[type='email'][data-choose-one!=-1]");
        if(queryset.length >= 2){
          var i = 1;
          while(true){
            var pair = [];
            queryset.each(function(){ if($(this).attr("data-choose-one") == i) pair.push($(this)); });
            if(pair.length < 2) break;
            var first = pair[0], second = pair[1];
            if(first.val().length == 0 && second.val().length == 0) {
              alert("Моля въведете едно от двете: " + first.attr("data-label") + ", " + second.attr("data-label"));
              count_bad_validation++;
            }
            i++;
          }
        }

        if(count_bad_validation > 0) {
          alert("Моля поправете полетата, оцветени в червено.");
          e.preventDefault(e);
        }
      });
     });

// Trigger custom front-end validation on leaving a text field
$('input[type="text"]').blur(function(){
  var value = $(this).val();
  if(value.length == 0) { // don't validate empty/unfilled fields
    $(this).parent().removeClass("has-error");
    $(this).prev().text("");
    $(this).attr("data-reason-failed", "None");
  } else if(value.length > 0) {
    var validation_type = $(this).attr("data-validation");
    if(validation_type != -1)
        handle_validation($(this), validation_type);
  }
});

function handle_validation(field, validation_type_id) {
  validation_type_id = parseInt(validation_type_id,10);
  var regex, error_message;
  switch(validation_type_id) {
    case 0: // phone
      regex = /^08\d{8}$/;
      error_message = "Моля въведете валиден телефон!";
      break;
    case 1: // postal code
      regex = /^\d{4}$/;
      error_message = "Моля въведете валиден пощенски код!";
      break;
    case 2: // name
      regex = /^[А-Я][а-яА-Я]+$/;
      error_message = "Моля въведете валидно име!";
      break;
    case 3: // number
      regex = /^-?\d+$/;
      error_message = "Моля въведете число!";
  }

  var matches = field.val().match(regex);
  if(matches) {
    if(field.attr("data-reason-failed") == "custom_validations") {
      field.parent().removeClass("has-error");
      field.prev().text("");
      field.attr("data-reason-failed", "None");
    }
  } else {
    field.parent().addClass("has-error");
    field.prev().text(error_message);
    field.attr("data-reason-failed", "custom_validations");
  }

  if(validation_type_id == 3) {
    var value = parseInt(field.val(), 10);
    var minimum = parseInt(field.attr("data-minimum"),10);
    var maximum = parseInt(field.attr("data-maximum"),10);

    if(minimum != -1) {
      if(maximum != -1) {
        if(value < minimum || value > maximum) {
          field.parent().addClass("has-error");
          error_message = "Моля въведете число в интервала " + minimum + "-" + maximum;
          field.prev(".help-block").text(error_message);
          field.attr("data-reason-failed", "bounds");
        } else {
          if(field.attr("data-reason-failed") == "bounds") {
            field.parent().removeClass("has-error");
            field.prev().text("");
            field.attr("data-reason-failed", "None");
          }
        }
      } else {
        if(value < minimum) {
          field.parent().addClass("has-error");
          error_message = "Моля въведете число по-голямо от " + minimum;
          field.prev().text(error_message);
          field.attr("data-reason-failed", "bounds");
        } else {
          if(field.attr("data-reason-failed") == "bounds") {
            field.parent().removeClass("has-error");
            field.prev().text("");
            field.attr("data-reason-failed", "None");
          }
        }
      }
    } else if(maximum != -1) {
      if(value > maximum) {
        field.parent().addClass("has-error");
        error_message = "Моля въведете число по-малко от " + maximum;
        field.prev().text(error_message);
        field.attr("data-reason-failed", "bounds");
      } else {
        if(field.attr("data-reason-failed") == "bounds") {
          field.parent().removeClass("has-error");
          field.prev().text("");
          field.attr("data-reason-failed", "None");
        }
      }
    }
  }
}

// Front-end validation for length of text fields
$('input[type="text"]').blur(function(){
  // alert($(this).attr("data-validation"));
  if($(this).attr("data-validation") != 3) {
    var length = $(this).val().length;
    var minimum = parseInt($(this).attr("data-minimum"),10);
    var maximum = parseInt($(this).attr("data-maximum"),10);
    if((minimum != -1 && length < minimum) || (maximum != -1 && length > maximum)) {
      error_message = "Дължината на това поле не може да е ";
      if(minimum != -1) error_message += "по-малка от " + minimum + " символа ";
      if(minimum != -1 && maximum != -1) error_message += " и ";
      if(maximum != -1) error_message += "по-голяма от " + maximum + " символа.";
      $(this).parent().addClass("has-error");
      $(this).prev().text(error_message);
      $(this).attr("data-reason-failed", "length");
    } else {
      if($(this).attr("data-reason-failed") == "length") {
        $(this).parent().removeClass("has-error");
        $(this).prev().text("");
        $(this).attr("data-reason-failed", "None");
      }
    }
  }
});
