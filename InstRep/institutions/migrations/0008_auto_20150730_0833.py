# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('institutions', '0007_auto_20150729_1122'),
    ]

    operations = [
        migrations.AlterField(
            model_name='institution',
            name='status',
            field=models.PositiveSmallIntegerField(choices=[(0, b'inactive'), (1, b'pending'), (2, b'active')]),
        ),
    ]
