import myapp.settings as settings
import os
import watson
from complaints.models import Complaint, File
from forms.models import Form
from institutions.models import Institution
from itertools import chain

def hash_taken(hash_value):
    return len(Complaint.objects.filter(hash_value=hash_value)) > 0

def gen_hash():
    hash_value = os.urandom(10).encode('hex')
    while hash_taken(hash_value):
        hash_value = os.urandom(10).encode('hex')
    return hash_value

def build_form_query_result(query):
    results = []
    institution_search = watson.filter(Institution, query)
    institution_capitalized_search = watson.filter(Institution, query.capitalize())
    institutions = list(chain(institution_search, institution_capitalized_search))

    for institution in institutions:
        for form in institution.form_set.all():
            results.append(form)

    form_search = watson.filter(Form, query)
    form_capitalized_search = watson.filter(Form, query.capitalize())
    forms = list(chain(form_search, form_capitalized_search))

    for form in forms:
        results.append(form)

    return set(results)

def get_path(user, complaint):
    username = user.user.username if user else 'anonymous'
    return "user_uploads/{0}/{1}/".format(username, complaint.id)

def write_to_destination(url, file):
    if not os.path.exists(url):
        os.makedirs('/'.join(url.split('/')[0:-1]))

    destination = open(url, "wb+")
    for chunk in file.chunks():
        destination.write(chunk)
    destination.close()

def handle_files(complaint, files, user):
    for key, file in files:
        new_file = File(complaint=complaint)
        new_file.field_name = key

        name_path = get_path(user, complaint)
        destination_url = settings.MEDIA_ROOT + '/' + name_path + file.name
        write_to_destination(destination_url, file)

        new_file.val.name = settings.MEDIA_URL + name_path + file.name
        new_file.save()

def create_complaint(form, user, post_data, files):
    new_complaint = Complaint.objects.create(
        form=form,
        user=user,
        status=0,
        hash_value=gen_hash()
    )

    for field in form.field_set.all():
        value = post_data.get(field.html_name)
        if value is not None and field.type != 6:
            new_complaint.fill_set.create(field_name=field.name, val=value)

    handle_files(new_complaint, files, user)

    return new_complaint

import ipware.ip
def get_ip(request):
    ip = ipware.ip.get_ip(request)
    if ip is not None:
        return ip
    else:
       return '-Not available-'

def read_file_content(file):
    content = ""
    for chunk in file.chunks():
        content += chunk
    return content

import base64
def files_to_dicts(form_name, files):
    form = Form.objects.get(name=form_name)
    file_fields = form.field_set.filter(type=6)
    dicts = []

    for file_field in file_fields:
        html_name = file_field.html_name
        actual_file = files.get(html_name, None)
        if actual_file:
            dicts.append({html_name: [{'name': actual_file.name, 'content': base64.b64encode(read_file_content(actual_file))}]})
        else:
            dicts.append({html_name: [{'name': '', 'content': ''}]})

    return dicts