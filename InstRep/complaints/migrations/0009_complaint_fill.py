# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('forms', '0006_auto_20150708_1020'),
        ('complaints', '0008_auto_20150708_1024'),
    ]

    operations = [
        migrations.CreateModel(
            name='Complaint',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('form', models.ForeignKey(to='forms.Form')),
            ],
        ),
        migrations.CreateModel(
            name='Fill',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('field_name', models.CharField(max_length=30)),
                ('val', models.CharField(max_length=200)),
            ],
        ),
    ]
