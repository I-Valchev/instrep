from django.test import TestCase


class test_imports(TestCase):

	def test_installed_apps(self):
		try:
			from django.apps import apps
			import myapp.settings
		except:
			self.fail("Import error: django apps")

		#self.assertEqual(apps.get_app_config('admin').name, 'django.contrib.admin')

		self.assertEqual(myapp.settings.INSTALLED_APPS, (
		    'django.contrib.admin',
			'django.contrib.auth',
			'django.contrib.contenttypes',
			'django.contrib.sessions',
			'django.contrib.messages',
			'django.contrib.staticfiles',
			'registration',
			'django.contrib.sites',
			'allauth',
			'allauth.account',
			'allauth.socialaccount',
			'allauth.socialaccount.providers.facebook',
			'django_nose',
			'forms',
			'complaints',)
		)

		try:
			from django.conf.urls import include, url
		except:
			self.fail("Import error: conf urls")

	def test_variables_key(self):
		try:
			import myapp.settings
		except:
			self.fail("Improt error: myapp.settigns")
			
		self.assertEqual(myapp.settings.SECRET_KEY, 'pzp1l0+c5he--l8(48x)us$qr5m8z=76gpz2)&3euyygklda-(')
		self.assertEqual(myapp.settings.MIDDLEWARE_CLASSES, ('django.contrib.sessions.middleware.SessionMiddleware', 
		    'django.middleware.common.CommonMiddleware', 
		    'django.middleware.csrf.CsrfViewMiddleware', 
		    'django.contrib.auth.middleware.AuthenticationMiddleware', 
		    'django.contrib.auth.middleware.SessionAuthenticationMiddleware', 
		    'django.contrib.messages.middleware.MessageMiddleware', 
		    'django.middleware.clickjacking.XFrameOptionsMiddleware', 
		    'django.middleware.security.SecurityMiddleware', ))

		self.assertEqual(myapp.settings.TEST_RUNNER, 'django_nose.NoseTestSuiteRunner')
		self.assertEqual(myapp.settings.DATABASES['default']['ENGINE'], 'django.db.backends.sqlite3')
		self.assertEqual(myapp.settings.LANGUAGE_CODE, 'en-us')
		self.assertEqual(myapp.settings.TIME_ZONE, 'UTC')
		self.assertTrue(myapp.settings.USE_I18N)
		self.assertTrue(myapp.settings.USE_L10N)
		self.assertTrue(myapp.settings.USE_TZ)
		self.assertEqual(myapp.settings.STATIC_URL, '/static/')