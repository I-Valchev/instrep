# -*- coding: utf-8 -*-
from django import forms
from django.core.exceptions import ValidationError
from django.forms import ModelForm
from institutions.models import Institution

class DocumentForm(forms.Form):
    docfile = forms.FileField(
        label='Select a file'
    )

class InstitutionProfileForm(forms.Form):
    inst_name = forms.CharField(max_length=200, label="Име на институцията")
    city = forms.CharField(max_length=30, label="Град")
    phone = forms.CharField(max_length=12, label="Телефон")
    notified = forms.BooleanField(required=False, label="Желаете ли да получавате ежедневен email със сигналите за деня?")

class InstitutionForm(forms.ModelForm):
    class Meta:
        model = Institution
        exclude = []
    
    def clean_user(self):
        user = self.cleaned_data['user']
        i = Institution.objects.filter(user=user)
        if len(i) == 1 and i[0].name == self.cleaned_data['name']:
        	return user
        if len(i) > 0:
            raise ValidationError("The user is already linked to another institution.")
        return user
