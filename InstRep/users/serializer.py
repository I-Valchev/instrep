from rest_framework import routers, serializers, viewsets
from .models import User
from .utils import update_profile

class UserSerializer(serializers.HyperlinkedModelSerializer):
    django_user_id = serializers.SerializerMethodField()
    django_user_username = serializers.SerializerMethodField()
    django_user_email = serializers.SerializerMethodField()
    permission_level = serializers.SerializerMethodField()
    
    class Meta:
        model = User
        fields = ('id', 'django_user_id', 'django_user_username', 'django_user_email', 'permission_level', 'first_name', 'last_name', 'city', 'phone', 'email', 'address',)

    def get_django_user_id(self, obj):
        return obj.user.id

    def get_django_user_username(self, obj):
        return obj.user.username

    def get_django_user_email(self, obj):
        return obj.user.email

    def get_permission_level(self, obj):
        return obj.permission_level

    def update(self, user_id, django_user, request, data):
        try:
            # data is {'old_pass': [u'password']}
            data = {attr: value[0] for attr, value in data.items()} # "flatten" the data
            return update_profile(request, user_id, django_user, data)
        except Exception as e:
            return {'status': 'error', 'error': str(e)}