from rest_framework import routers, serializers, viewsets
from .models import Complaint, Fill, File, Comment

class CommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        exclude = ('id', 'complaint')

class FileSerializer(serializers.ModelSerializer):
    class Meta:
        model = File
        exclude = ('id', 'complaint', )

class FillSerializer(serializers.ModelSerializer):
    class Meta:
        model = Fill
        exclude = ('id', 'complaint', )

class ComplaintSerializer(serializers.ModelSerializer):
    form_name = serializers.SerializerMethodField()
    fill_set = FillSerializer(many=True, read_only=True)
    file_set = FileSerializer(many=True, read_only=True)
    comment_set = CommentSerializer(many=True, read_only=True)

    def update(self, complaint_id, data, user):
        try:
            complaint = Complaint.objects.get(pk=complaint_id)
            institution_user = complaint.form.institution.user.user
            if institution_user != user:
                return {'status': 'error', 'error': 'This complaint does not belong to you.'}
            
            status = data.pop('status', None)
            if not status:
                return {'status': 'error', 'error': 'No status is specified.'}
            
            status = int(status[0])
            if status < 0 or status > 2:
                return {'status': 'error', 'error': 'Illegal status. It must be 0, 1 or 2.'}

            complaint.status = status
            complaint.save(update_fields=['status'])

            return {'status': 'success'}
        except Exception as e:
            return {'status': 'error', 'error': str(e)}

    class Meta:
        model = Complaint
        fields = ('id', 'status', 'form_name', 'created_at', 'updated_at', 'hash_value', 'fill_set', 'file_set', 'comment_set')

    def get_form_name(self, obj):
        if obj.form:
            return obj.form.name
        else:
            return 'deleted form'
