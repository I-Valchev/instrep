from django.db import models

class Form(models.Model):
    inst_name = models.CharField(max_length = 200)

class Field(models.Model):
    form = models.ForeignKey(Form)
    type = models.CharField(max_length = 20)
    name = models.CharField(max_length = 20)
    
class FieldOption(models.Model):
    field_id = models.ForeignKey(Field)
    option = models.CharField(max_length = 50)