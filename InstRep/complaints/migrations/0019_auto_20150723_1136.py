# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0002_auto_20150722_0629'),
        ('complaints', '0018_auto_20150723_0734'),
    ]

    operations = [
        migrations.AddField(
            model_name='complaint',
            name='user',
            field=models.ForeignKey(default=-1, to='users.User'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='complaint',
            name='status',
            field=models.PositiveSmallIntegerField(choices=[(0, b'Pending'), (1, b'Will not be adressed'), (2, b'Resolved')]),
        ),
    ]
