# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('complaints', '0012_auto_20150708_1027'),
    ]

    operations = [
        migrations.RenameField(
            model_name='complaint',
            old_name='form_id',
            new_name='form',
        ),
    ]
