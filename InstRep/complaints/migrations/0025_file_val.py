# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import complaints.models


class Migration(migrations.Migration):

    dependencies = [
        ('complaints', '0024_auto_20150730_2316'),
    ]

    operations = [
        migrations.AddField(
            model_name='file',
            name='val',
            field=models.FileField(default=-1, upload_to=complaints.models.generate_filename),
            preserve_default=False,
        ),
    ]
