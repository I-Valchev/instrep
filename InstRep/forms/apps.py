# -*- coding: utf-8 -*-
from django.apps import AppConfig
import watson

class FormConfig(AppConfig):
    name = 'forms'
    verbose_name = u'Форми'

    def ready(self):
        Form = self.get_model('Form')
        watson.register(Form.objects.filter(approved=True), fields=('name',))