# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('forms', '0023_auto_20150810_1240'),
    ]

    operations = [
        migrations.RenameField(
            model_name='field',
            old_name='field_type',
            new_name='type',
        ),
    ]
