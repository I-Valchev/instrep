# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('institutions', '0008_auto_20150730_0833'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='institution',
            options={'verbose_name': '\u0438\u043d\u0441\u0442\u0438\u0442\u0443\u0446\u0438\u044f', 'verbose_name_plural': '\u0438\u043d\u0441\u0442\u0438\u0442\u0443\u0446\u0438\u0438'},
        ),
    ]
