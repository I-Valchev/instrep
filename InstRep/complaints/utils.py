def build_context_data(complaint):
    return {
        'complaint': complaint,
        'fills': complaint.fill_set.all(),
        'files': complaint.file_set.all(),
        'comments': complaint.comment_set.all()
    }