# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('forms', '0040_auto_20150912_1421'),
    ]

    operations = [
        migrations.AddField(
            model_name='field',
            name='hidden',
            field=models.BooleanField(default=False, verbose_name=b'\xd0\xa1\xd0\xba\xd1\x80\xd0\xb8\xd1\x82\xd0\xbe \xd0\xbf\xd0\xbe\xd0\xbb\xd0\xb5'),
        ),
    ]
