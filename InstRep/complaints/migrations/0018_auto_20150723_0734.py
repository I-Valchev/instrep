# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('complaints', '0017_merge'),
    ]

    operations = [
        migrations.AddField(
            model_name='complaint',
            name='created_at',
            field=models.DateTimeField(default=datetime.datetime(2015, 7, 23, 7, 34, 16, 833288, tzinfo=utc), auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='complaint',
            name='updated_at',
            field=models.DateTimeField(default=datetime.datetime(2015, 7, 23, 7, 34, 20, 118813, tzinfo=utc), auto_now=True),
            preserve_default=False,
        ),
    ]
