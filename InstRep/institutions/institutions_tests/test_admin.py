# -*- coding: UTF-8 -*-

from django.test import TestCase
from institutions.admin import *
from institutions.models import Institution
from users.models import User
from django.contrib.auth.models import User as DjangoUser
from institutions.forms import InstitutionForm
from django.contrib.admin.options import ModelAdmin
from django.contrib.admin.sites import AdminSite
from django.test.client import RequestFactory
from django.http import Http404
from mock import patch

class test_institutions_admin(TestCase):
    def setUp(self):
        self.request_factory = RequestFactory()
        mock_django_user = DjangoUser.objects.create_user('test_user', 'test@testmail.com', 'test_password')
        self.mock_user = User.objects.create(user=mock_django_user, permission_level=2)
        self.inst = Institution(name='TestInst', user=self.mock_user, status=2)
        self.inst.save()
        self.site = AdminSite()
        self.mock_model_admin = ModelAdmin(self.inst,self.site)
        self.inst_admin = InstitutionAdmin(self.inst,self.mock_model_admin)

    def test_activate_inst_wrong_request(self):
        request = 'fake_request'
        queryset = [self.inst]

        self.assertRaises(Http404, self.inst_admin.activate_inst, request, queryset)

    def test_activate_inst(self):
        self.inst.status = 1
        queryset = [self.inst]
        request = self.request_factory.post('', data={})

        self.inst_admin.activate_inst(request, queryset)

        self.assertEqual(queryset[0].status, 2)

    def test_get_actions_wrong_request(self):
        request = 'fake_request'

        self.assertRaises(Http404, self.inst_admin.get_actions, request)

    def test_get_actions_no_superuser(self):
        mock_model_admin = ModelAdmin(self.inst,self.site)
        inst_admin = InstitutionAdmin(self.inst,mock_model_admin)
        request = self.request_factory.post('', data={})
        request.user = self.mock_user
        request.user.is_superuser = False

        result = self.inst_admin.get_actions(request)

        self.assertTrue(self._is_empty(result))

    def test_get_actions(self):
        mock_model_admin = ModelAdmin(self.inst,self.site)
        inst_admin = InstitutionAdmin(self.inst,mock_model_admin)
        request = self.request_factory.post('', data={})
        request.user = self.mock_user
        request.user.is_superuser = True

        result = self.inst_admin.get_actions(request)
        self.assertEqual(result['activate_inst'][2], u'\u0410\u043a\u0442\u0438\u0432\u0438\u0440\u0430\u0439 \u0438\u043d\u0441\u0442\u0438\u0442\u0443\u0446\u0438\u044f')

    @patch('institutions.admin.send_mail')
    def test_send_mail(self, mock_send):
        request = self.request_factory.post('', data={})
        self.inst.status = 2
        self.inst_admin.save_model(request, self.inst, self.inst, '')

        expected_subject = 'Активация на акаунт в ПодайСигнал'
        expected_body = 'Вашият акаунт като институция беше потвърден!'\
            ' Вече можете да създавате форми и да отговаряте на оплаквания.'
        expected_sender = 'stamaniorec@gmail.com'

        mock_send.assert_called_with(expected_subject, expected_body, expected_sender, [self.inst.user.user.email])

    def _is_empty(self, struct):
        if not struct:
            return True
        return False
