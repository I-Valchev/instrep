# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('forms', '0014_merge'),
    ]

    operations = [
        migrations.AddField(
            model_name='form',
            name='created_at',
            field=models.DateTimeField(default=datetime.datetime(2015, 7, 23, 7, 34, 25, 496522, tzinfo=utc), auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='form',
            name='updated_at',
            field=models.DateTimeField(default=datetime.datetime(2015, 7, 23, 7, 34, 29, 111443, tzinfo=utc), auto_now=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='field',
            name='name',
            field=models.CharField(max_length=100),
        ),
    ]
