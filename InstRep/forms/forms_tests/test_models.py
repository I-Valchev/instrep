# -*- coding: UTF-8 -*-

from django.test import TestCase
from forms.models import Form
from users.models import User
from django.contrib.auth.models import User as DjangoUser
from institutions.models import Institution

  
class test_models(TestCase):
    def setUp(self):
    	mock_django_user = DjangoUser.objects.create_user(username='test_user', 
            email='test@testmail.com', password='test_password')
        mock_user = User.objects.create(user=mock_django_user, permission_level=2)
        mock_user.save()
    	inst = Institution(user=mock_user, name='TestInst', status=2)
        inst.save()
        self.form = Form(institution=inst, name='fake_form')
        self.form.save()

    def test_str(self):
    	self.assertEqual(str(self.form), 'fake_form')

