# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('complaints', '0002_auto_20150707_1052'),
    ]

    operations = [
        migrations.RenameField(
            model_name='complaints',
            old_name='form_id',
            new_name='form',
        ),
    ]
