# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('forms', '0043_auto_20151215_1257'),
    ]

    operations = [
        migrations.AlterField(
            model_name='form',
            name='name',
            field=models.CharField(max_length=150, verbose_name=b'\xd0\x98\xd0\xbc\xd0\xb5 \xd0\xbd\xd0\xb0 \xd1\x84\xd0\xbe\xd1\x80\xd0\xbc\xd0\xb0\xd1\x82\xd0\xb0'),
        ),
    ]
