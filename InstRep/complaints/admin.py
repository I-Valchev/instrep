# -*- coding: utf-8 -*-
from django.contrib import admin
from nested_inline.admin import NestedStackedInline, NestedModelAdmin
from complaints.models import Complaint, Fill, File, Comment
from users.models import User
from myapp.utils import validate_request

class FillInline(admin.StackedInline):
    model = Fill
    extra = 0
    fields = ('field_name', 'val')

    def get_readonly_fields(self, request, obj=None):
        validate_request(request, 'GETPOST')
        if request.user.is_superuser:
            return super(FillInline, self).get_readonly_fields(request, obj)
        else:
            return ('val', 'field_name')

class FileInline(admin.StackedInline):
    model = File
    extra = 0

    def get_readonly_fields(self, request, obj=None):
        validate_request(request, 'GETPOST')
        if request.user.is_superuser:
            return super(FileInline, self).get_readonly_fields(request, obj)
        else:
            return ('val', 'field_name')

class CommentViewInline(admin.StackedInline):
    model = Comment
    extra = 1

    def get_readonly_fields(self, request, obj=None):
        validate_request(request, 'GETPOST')
        return ('text',)

    def has_add_permission(self, request):
        validate_request(request, 'GETPOST')
        return False

class CommentAddInline(admin.StackedInline):
    model = Comment
    extra = 1
    max_num = 1

    def has_change_permission(self, request, obj=None):
        validate_request(request, 'GETPOST')
        return False

class ComplaintAdmin(admin.ModelAdmin):
    model = Complaint
    extra = 0
    inlines = [FillInline, FileInline, CommentViewInline, CommentAddInline]
    list_display = ('id', '__unicode__', 'created_at', 'status',)

    def get_queryset(self, request):
        validate_request(request, 'GETPOST')
        qs = super(ComplaintAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs

        return qs.filter(form__institution__user=User.objects.get(user=request.user))

    def get_readonly_fields(self, request, obj=None):
        validate_request(request, 'GETPOST')
        if request.user.is_superuser:
            return super(ComplaintAdmin, self).get_readonly_fields(request, obj)
        else:
            return ('user', 'form', 'hash_value')

    add_form_template = 'admin/forms/form/add_form.html'
    def add_view(self,request, form_url='', extra_context=None):
        return super(ComplaintAdmin, self).add_view(request, form_url, extra_context)

    def get_actions(self, request):
        validate_request(request, 'GETPOST')
        actions = super(ComplaintAdmin, self).get_actions(request)
        if not request.user.is_superuser:
            if 'delete_selected' in actions:
                del actions['delete_selected']
        return actions

admin.site.register(Complaint, ComplaintAdmin)
