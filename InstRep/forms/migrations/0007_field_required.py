# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('forms', '0006_auto_20150708_1020'),
    ]

    operations = [
        migrations.AddField(
            model_name='field',
            name='required',
            field=models.BooleanField(default=False),
        ),
    ]
