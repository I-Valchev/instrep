# -*- coding: utf-8 -*-
from django.contrib import admin
from forms.models import Form, Field, FieldOption
from users.models import User
from institutions.models import Institution
from django.core.urlresolvers import reverse
from django.conf.urls import url
from django.shortcuts import redirect, render
from django.contrib import messages
from myapp.utils import validate_request

from django.contrib.admin import TabularInline, StackedInline, site
from super_inlines.admin import SuperInlineModelAdmin, SuperModelAdmin

import re

class FieldOptionInline(SuperInlineModelAdmin, StackedInline):
    model = FieldOption
    extra = 0
    fk_name = "field"

class FieldInline(SuperInlineModelAdmin, StackedInline):
    model = Field
    fk_name="form"
    inlines = [FieldOptionInline]
    exclude = ['choose_one']

    def get_extra(self, request, obj=None, **kwargs):
        # /admin/forms/form/add/
        if re.match(r'/admin/forms/form/add/', request.path):
            return 1
        # /admin/forms/form/37/
        else:
            return 0

class FormAdmin(SuperModelAdmin):
    model = Form
    inlines = [FieldInline]
    list_display = ('name', 'approved')
    list_filter = ('institution','approved')
    actions = ['approve_form']

    def approve_form(self, request, queryset):
        validate_request(request, 'POST')
        queryset.update(approved=True)
    approve_form.short_description = u'Одобри формата'

    def get_actions(self, request):
        validate_request(request, 'GETPOST')
        actions = super(FormAdmin, self).get_actions(request)
        if not request.user.is_superuser:
            del actions['approve_form']
        return actions

    def get_urls(self):
        urls = super(FormAdmin, self).get_urls()
        my_urls = [
            url(r'^(?P<form_id>[0-9]+)/main_field_select', self.main_field_select_view),
        ]
        return my_urls + urls

    def main_field_select_view(self, request, form_id):
        validate_request(request, 'GETPOST')
        form = Form.objects.get(pk=form_id)
        fields = form.field_set.all()
        if request.method == 'POST':
            form.main_field = request.POST.get('main_field_name')
            form.save()
            messages.success(request, 'Successfully created/updated form.')
            return redirect('/admin/forms')
        return render(request, 'admin/forms/form/main_field_select.html', {'form': form, 'fields': fields})

    # Fields in show action
    def get_fields(self, request, obj=None):
        validate_request(request, 'GETPOST')
        if request.user.is_superuser:
            return super(FormAdmin, self).get_fields(request,obj)
        else:
            return ('name',)

    # Where to redirect on object creation
    def response_add(self, request, obj):
        return redirect("/admin/forms/form/%d/main_field_select_view" % obj.id)

    # Where to redirect on change
    def response_change(self, request, obj):
        return redirect("/admin/forms/form/%d/main_field_select_view" % obj.id)

    # Called after clicking "Save" and before saving to the DB
    def save_model(self, request, obj, form, change):
        validate_request(request, 'POST')
        if not request.user.is_superuser:
            obj.institution = Institution.objects.get(user=(User.objects.get(user=request.user)))
        obj.save()

    # Limit what to list in index
    def get_queryset(self, request):
        validate_request(request, 'GETPOST')
        qs = super(FormAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs

        return qs.filter(institution__user=User.objects.get(user=request.user))

    def get_readonly_fields(self, request, obj=None):
        validate_request(request, 'GETPOST')
        if request.user.is_superuser:
            return super(FormAdmin, self).get_readonly_fields(request, obj)
        else:
            return ('institution', 'main_field')

admin.site.register(Form, FormAdmin)
