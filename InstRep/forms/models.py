# -*- coding: utf-8 -*-
from django.db import models
from institutions.models import Institution

FIELD_TYPES = (
    (1, 'Текстово поле'), 
    (2, 'Текст'), 
    (3, 'Падащо поле'),
    (4, 'Бутони за единичен избор'),
    (5, 'Email'),
    (6, 'Файл'),
    (7, 'Кутийка за избор'),
    (8, 'Скрито поле'),
)

AUTOFILL_NAMES = (
    (-1, '-'),
    (0, 'Лично име'), 
    (1, 'Фамилия'), 
    (2, 'Град'), 
    (3, 'Телефон'), 
    (4, 'Email'), 
    (5, 'Адрес'),                  
)

VALIDATION_TYPES = (
    (-1, '-'),
    (0, 'Телефон'), 
    (1, 'Пощенски код'), 
    (2, 'Име'),
    (3, 'Число'),
)

class Form(models.Model):
    institution = models.ForeignKey(Institution)
    name = models.CharField(max_length=150, verbose_name="Име на формата")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    main_field = models.CharField(max_length=200, null=True, blank=True)
    approved = models.BooleanField(default=False, verbose_name="Одобрена")

    class Meta:
        verbose_name = 'форма'
        verbose_name_plural = 'форми'

    def __unicode__(self):
        return u'%s' % self.name

class Field(models.Model):
    form = models.ForeignKey(Form)
    type = models.PositiveSmallIntegerField(choices = FIELD_TYPES, verbose_name="Тип на полето")
    name = models.CharField(max_length = 100, verbose_name="Име на полето")
    html_name = models.CharField(default="", max_length=50, verbose_name="HTML name атрибут")
    description = models.TextField(blank=True, default="", verbose_name="Описание на полето")
    autofill = models.PositiveSmallIntegerField(choices = AUTOFILL_NAMES, default=-1, \
        verbose_name="Автоматично попълване", \
        help_text="Ако потребителят е попълнил профила си с въпросната информация, полето ще се попълни автоматично." \
    )
    required = models.BooleanField(default=False, verbose_name="Задължително")
    validation_type = models.PositiveSmallIntegerField(choices=VALIDATION_TYPES, default=-1, verbose_name="Тип на валидацията")
    minimum = models.IntegerField(default=-1, blank=True, null=True, verbose_name="Минимална големина")
    maximum = models.IntegerField(default=-1, blank=True, null=True, verbose_name="Максимална големина")
    choose_one = models.PositiveSmallIntegerField(blank=True, default=-1)

    field_order = models.PositiveIntegerField(default=0, db_index=True)

    class Meta:
        verbose_name = u'поле'
        verbose_name_plural = u'полета'
        ordering = ['field_order']
    
class FieldOption(models.Model):
    field = models.ForeignKey(Field)
    option = models.CharField(max_length = 50)

    field_option_order = models.PositiveIntegerField(default=0, db_index=True)

    class Meta:
        verbose_name = u'опция'
        verbose_name_plural = u'опции'
        ordering = ['field_option_order']
