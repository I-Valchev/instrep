# -*- coding: utf-8 -*-
from django.apps import AppConfig
import watson

class InstitutionConfig(AppConfig):
    name = 'institutions'
    verbose_name = u'Институции'

    def ready(self):
        Institution = self.get_model('Institution')
        watson.register(Institution.objects.filter(status=2), fields=('name',))