"""myapp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    url(r'^o/', include('oauth2_provider.urls', namespace='oauth2_provider')),
    url(r'^admin/inst_profile', 'myapp.views.inst_profile', name='inst_profile'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', 'myapp.views.home'),
    url(r'^profile', 'users.views.profile', name='profile'),
    url(r'^dashboard', 'users.views.dashboard', name='dashboard'),
    url(r'^submit/(?P<user_id>[0-9]+)$', 'users.views.submit', name='submit'),
    url(r'^accounts/', include('auth.urls')),
    url(r'^forms/', include('forms.urls', namespace="forms")),
    url(r'^complaints/', include('complaints.urls', namespace="complaints")),
    url(r'^upload/(?P<institution_id>[0-9]+)', 'institutions.views.upload', name='upload'),
    url(r'^thanks_for_submission', 'institutions.views.thanks_for_submission', name='thanks_for_submission'),
    url(r'^institutions/leaderboard', 'institutions.views.leaderboard', name='leaderboard'),
    url(r'^api/', include('api.urls')),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
] 
if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) 
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
