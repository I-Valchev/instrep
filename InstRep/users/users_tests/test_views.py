# T-*- coding: UTF-8 -*-

import unittest
from django.test import TestCase
from django.test.client import RequestFactory
from mock import patch
import users.views as view
from users.models import User
from django.contrib.auth.models import User as DjangoUser
from django.test.client import Client
from django.http.response import HttpResponse, Http404
from django.utils.functional import SimpleLazyObject

class test_views(TestCase):

    def setUp(self):
        self.client = Client()
        self.request_factory = RequestFactory()
        self.username = 'agconti'
        self.email = 'test@test.com'
        self.password = 'test'
        self.test_django_user = DjangoUser.objects.create_user(self.username, self.email, self.password)
        self.test_django_user.save()
        self.test_user = User.objects.create(user=self.test_django_user, permission_level=1)
        self.test_user.save()

    @patch('django.contrib.auth.models.User.is_authenticated')
    def test_is_regular_user(self, mock_auth):
        mock_auth.return_value= True
        self.assertTrue(view.is_regular_user(self.test_django_user))

    @patch('django.contrib.auth.models.User.is_authenticated')
    def test_dashboard_unauthenticated(self, mock_auth):
        logout = self.client.logout()
        self.assertEqual(logout, None)
        response = self.client.get('/dashboard/')
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response,'/accounts/login/?next=/dashboard/')
        self.assertTemplateNotUsed(response, 'users/profile_dashboard.html')

    def test_dashboard_authenticated(self):
        login = self.client.login(username=self.username, password=self.password)
        self.assertTrue(login)
        response = self.client.get('/dashboard/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'users/profile_dashboard.html')

    @patch('users.utils.change_password')
    @patch('users.views.messages')
    @patch('users.views.is_regular_user')
    def test_submit_change_details(self, mock_is_regular, mock_message, mock_change_password):
        mock_is_regular.return_value = True
        mock_message.success.return_value = True
        mock_change_password.return_value = True
        request = self.request_factory.post('', data={
            'first_name':'fake_first', 'last_name':'fake_last', 'city':'London',
            'phone':'0123456789', 'email':'fake@mail.com', 'address':'where_i_live',
            'new_pass':'new_fake_password', 'old_pass':self.password, })
        request.user = self.test_user.user

        response = view.submit(request, 1)
        self.test_django_user = DjangoUser.objects.get(id=1)
        self.test_user = User.objects.get(user=self.test_django_user)


        self.assertEqual(response.status_code, 200)
        mock_message.success.assert_called_with(request, 'Профилът е обновен успешно!')
        self.assertEqual(self.test_user.first_name, 'fake_first')
        self.assertEqual(self.test_user.last_name, 'fake_last')
        self.assertEqual(self.test_user.city, 'London')
        self.assertEqual(self.test_user.phone, '0123456789')
        self.assertEqual(self.test_user.email, 'fake@mail.com')
        self.assertEqual(self.test_user.address, 'where_i_live')
        mock_change_password.assert_called_with(request, self.test_user, 'new_fake_password')

    @patch('users.utils.change_password')
    @patch('users.views.messages')
    @patch('users.views.is_regular_user')
    def test_submit_wrong_current_password(self, mock_is_regular, mock_message, mock_change_password):
        mock_is_regular.return_value = True
        mock_message.error.return_value = True
        request = self.request_factory.post('', data={
            'first_name':'fake_first', 'last_name':'fake_last', 'city':'London',
            'phone':'0123456789', 'email':'fake@mail.com', 'address':'where_i_live',
            'new_pass':'new_fake_password', 'old_pass':'wrong_pass', })
        request.user = self.test_user.user

        view.submit(request, 1)
        mock_message.error.assert_called_with(request, 'Грешна парола!')
        self.assertFalse(mock_change_password.called)

    def test_profile_authenticated(self):
        login = self.client.login(username=self.username, password=self.password)
        self.assertTrue(login)
        response = self.client.get('/profile/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'users/profile.html')

    def test_profile_unauthenticated(self):
        logout = self.client.logout()
        response = self.client.get('/profile/')
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/accounts/login/?next=/profile/')
        self.assertTemplateNotUsed(response, 'users/profile.html')
