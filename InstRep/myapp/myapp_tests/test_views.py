from django.test import TestCase
from django.http.response import Http404, HttpResponse
from myapp.views import *
from django.test.client import RequestFactory

class test_views(TestCase):

	def setUp(self):
		self.request_factory = RequestFactory()

	def test_home(self):
		request = self.request_factory.get('', data={})
		self.assertIsInstance(home(request), HttpResponse)

		request = 'fake request'
		self.assertRaises(home(request), Http404)

		request = self.request_factory.post('', data={})
		self.assertRaises(home(request), Http404)

	def test_inst_profile(self):
		
