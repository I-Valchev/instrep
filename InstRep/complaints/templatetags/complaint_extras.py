import complaints.models as complaints

from django import template
register = template.Library()

@register.filter
def get_status(status_id):
    try:
        return complaints.COMPLAINT_STATUSES[status_id][1]
    except:
        return None

@register.assignment_tag
def is_hidden(fill):
    form = fill.complaint.form
    if form:
        for form_field in form.field_set.all():
            if form_field.type == 8:
                if fill.field_name == form_field.name:
                    return True
    return False