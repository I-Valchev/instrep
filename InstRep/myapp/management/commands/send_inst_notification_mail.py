# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from institutions.models import Institution
from django.core.mail import EmailMessage
from django.utils import timezone

from myapp.settings import ALLOWED_HOSTS

def send_mail(subject, message, sender, to_list):
    msg = EmailMessage(subject, message, sender, to_list)
    msg.content_subtype = 'html'
    return msg.send()

class Command(BaseCommand):
    help = 'Sends daily email with report of all complaints to the institution from that day'
    
    def handle(self, *args, **options):
        for institution in Institution.objects.all():
            if institution.notified == True:
                email = institution.user.user.email
                email_str = self.build_email_string(institution)

                send_mail('MyApp Daily Report', email_str, 'stamaniorec@gmail.com', [email])

    def build_email_string(self, institution):
        email_str = u"<h1>Здравейте %s, това е дневния ви доклад.</h1>\n" % institution.name
        email_str += u"Днешна дата: %s" % (timezone.now().date().strftime("%-d %B %Y"))

        for form in institution.form_set.all():
            email_str += self.build_form_summary(form)

        return email_str

    def build_form_summary(self, form):
        if form.approved:
            form_summary = (u"\nЗа форма \"%s\":\n" % form.name)

            form_summary += "<ul>\n"
            for complaint in form.complaint_set.all():
                form_summary += self.build_complaint_summary(complaint, form.main_field)
            form_summary += "</ul>\n\n"
        else:
            form_summary = ''

        return form_summary

    def build_complaint_summary(self, complaint, main_field):
        if complaint.created_at.date() == timezone.now().date():
            main_field = complaint.fill_set.get(field_name=main_field)
            complaint_str = ("<a href=\"http://agile-garden-2320.herokuapp.com/admin/complaints/complaint/%s\">" % complaint.id)
            complaint_str += ("%s: %s" % (main_field.field_name, main_field.val))
            complaint_str += "</a>"
            return ("<li>%s</li>\n" % complaint_str)
        else:
            return ''