# -*- coding: utf-8 -*-
from django.contrib import messages
from django.http.response import Http404
from django.shortcuts import redirect
from django.shortcuts import render
from .models import Complaint, Comment
from .utils import build_context_data
from myapp.utils import validate_request

def show(request, complaint_id):
    validate_request(request, 'GETPOST')
    
    complaint = Complaint.objects.get(id=complaint_id)
    if request.method == 'POST':
        comment_text = request.POST.get('comment_text')
        Comment.objects.create(complaint=complaint, text=comment_text)

    return render(request, 'complaints/show.html', build_context_data(complaint))

def hash_show(request, hash_value):
    complaint = Complaint.objects.get(hash_value=hash_value)
    if request.method == 'POST':
        comment_text = request.POST.get('comment_text')
        Comment.objects.create(complaint=complaint, text=comment_text)

    return render(request, 'complaints/show.html', build_context_data(complaint))
