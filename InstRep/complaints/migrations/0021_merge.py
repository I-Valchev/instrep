# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('complaints', '0020_auto_20150724_1132'),
        ('complaints', '0020_merge'),
    ]

    operations = [
    ]
